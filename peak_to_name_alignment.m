function Peaks=peak_to_name_alignment(chrom,standard)

% clear
% clc
% close all
% relabel='p308 Ec mrdA K12 KO 061512';
% load('Standard_MG1655_071812.mat');
% load([relabel '.mat']);
% load the standard to compare against, keep track of peak interval
% position
% load the file to be relabed
h=waitbar(0,'Comparing chromatograms...');


if exist('chrom','var')
    t2 = chrom.t';
    chrom = chrom.final(:,1)';
else
    chrom = chrom2.final(:,1)';
    t2 = chrom2.t';
    clear chrom2
end

chrom2=chrom;
clear chrom

load(standard);
Pint=zeros(size(Peaks,2),3);
for i = 1: size(Peaks,2)
    if exist('Peaks(i).position','var')
        Pint(i,1) = Peaks(i).position(1);
    else
        Pint(i,1) =(Peaks(i).interval(1)+Peaks(i).interval(2))/2;
    end
    Pint(i,2:3) = Peaks(i).interval;
end
%%CT 92413
[B,IX]=sort(Pint(:,2));
Pint=Pint(IX,:);
Peaks=Peaks(IX);
%%CT 92413

chrom1 = chrom.final(:,1)';
t1 = chrom.t';
clear chrom
% downsample the chromatograms
npts = 50;
t1 = downsample(t1,npts);
t2 = downsample(t2,npts);
dt1 = t1(2)-t1(1);
dt2 = t2(2)-t2(1);
chrom1 = downsample(chrom1,npts);
chrom2 = downsample(chrom2,npts);
init = 1;
endend = min(min(numel(chrom1),numel(chrom2)),320);
chrom1=chrom1(init:endend);
chrom2=chrom2(init:endend);
%normalize the chromatograms so they are easier to compare
chrom1=chrom1/max(chrom1);
chrom2=chrom2/max(chrom2);
t1=t1(init:endend);
t2=t2(init:endend);
cutoff = 0.01;
% figure;
% plot(t1,chrom1,t2,chrom2);

%% Convert chromatograms into string of Q (tall peaks), P (less tall peaks) and N (baseline)
str1 = [];
str2 = [];
for i = 1 : size(chrom1,2)
    if chrom1(i)> 0.2
        str1=cat(2,str1,'Q');
    elseif chrom1(i)>cutoff
        str1=cat(2,str1,'P');
    else
        str1=cat(2,str1,'N');
    end
end
for j = 1 : size(chrom2,2)
    if chrom2(j)>0.2
        str2=cat(2,str2,'Q');
    elseif chrom2(j)>cutoff
        str2=cat(2,str2,'P');
    else
        str2=cat(2,str2,'N');
    end
end

% disp(str1)
% disp(str2)
% Matrices that define the alignment costs and gains
r = ['N' 'P' 'Q'];
s = [10 -10 -50; -10 50 70; -50 70 200];
d = 8; %gap cost
%initialize the matrix of all alignments and directionality to make the
%alignment
waitbar(0.2,h,'Sliding peak assignments...');
F = zeros(size(str1,2)+1,size(str2,2)+1); %this is the matrix of maximized scores
orig = zeros(size(str1,2)+1,size(str2,2)+1); %this is where the move originated from
% initialize first row and first column to be just gaps
for i = 1 : size(F,1)%str1
    F(i,1) = -(i-1)*d;
    orig(i,1) = 2; % index 2: gap in str 2
end
for j = 1 : size(F,2) %str2
    F(1,j) = -(j-1)*d;
    orig(1,j) = 3;% index 3: gap in str 1
end

str1c = cat(2,str1,'N'); %add an N at the end so that indexing works
str2c = cat(2,str2,'N'); %add an N at the end so that indexing works

%calculate the alignment matrix
for i = 2 : size(F,1)
    str1_char = find(r==str1c(i-1));
    for j = 2 : size(F,2)
        str2_char = find(r==str2c(j-1));
        if strcmp(str1c(i-1),'P') && strcmp(str1c(i),'P') %if in the middle of a peak adding a gap costs more
            d3 = 5*d;
            %if in the middle of a tall peak adding a gap costs even more
        elseif strcmp(str1c(i-1),'Q') && (strcmp(str1c(i),'P') || strcmp(str1c(i),'Q'))
            d3 = 10*d;
        else
            d3 = d;
        end
        if strcmp(str2c(j-1),'P') && strcmp(str2c(j),'P')
            d2 = 5*d;
        elseif strcmp(str2c(j-1),'Q') && (strcmp(str2c(j),'P') || strcmp(str2c(j),'Q'))
            d2 = 10*d;
        else
            d2 = d;
        end
        % m is a size 3 vector of scores for:
        m = [F(i-1,j-1)+s(str1_char,str2_char),...% index 1: direct alignment
            F(i-1,j)-d2,...% index 2: gap in str2
            F(i,j-1)-d3];% index 3: gap in str1
        [ms, idx] = sort(m);
        F(i,j) = ms(3);
        orig(i,j) = idx(3);
    end
end
waitbar(0.5,h,'Sliding peak assignments...');
% align sequences based on scoring and traceback
i = size(F,1);
j = size(F,2);
str1a='';
str2a='';
t1_new = t1;
while i>1&&j>1%~(i==1&&j==1)
    if orig(i,j)==1
        i = i-1;
        j = j-1;
        str1a = cat(2,str1(i),str1a);
        str2a = cat(2,str2(j),str2a);
    elseif orig(i,j)==2
        i = i-1;
        Pint(Pint(:,2)>=t2(j-1)|Pint(:,3)>=t2(j-1),2:3) = Pint(Pint(:,2)>=t2(j-1)|Pint(:,3)>=t2(j-1),2:3) - dt2;
        str1a = cat(2,str1(i),str1a);
        str2a = cat(2,'-',str2a);
    else
        j = j-1;
        Pint(Pint(:,2)>=t1(i-1)|Pint(:,2)>=t1(i-1),2:3) = Pint(Pint(:,2)>=t1(i-1)|Pint(:,2)>=t1(i-1),2:3) + dt1;
%         Pint(Pint>=t1(i-1)) = Pint(Pint>=t1(i-1)) + dt1;
        t1_new = cat(2,t1_new(1:i-1),t1_new(i-1)+dt1,t1_new(i:end)+dt1);
        str1a = cat(2,'-',str1a);
        str2a = cat(2,str2(j),str2a);
    end
end
% disp(str1a)
% disp(str2a)
Peaks_std = Peaks;
waitbar(0.8,h,'Setting up optimized labels...');
%% label the new peaks
for i = 1: size(Pint,1)
    Peaks(i).position=Pint(i,1);
    Peaks(i).interval=Pint(i,2:3);
end

% save(['Peaks_' relabel '.mat'],'Peaks');
%%
chrom1a = [];
chrom2a = [];
chrom1_interim = chrom1;
chrom2_interim = chrom2;
for i = 1 : size(str1a,2)
    if strcmp(str1a(i),'-')
        chrom1_interim = [chrom1_interim(1:i-1) chrom1_interim(i-1) chrom1_interim(i:end)];
    end
    chrom1a=cat(2,chrom1a,chrom1_interim(i));
end
for j = 1 : size(str2a,2)
    if strcmp(str2a(j),'-')
        chrom2_interim = [chrom2_interim(1:j-1) chrom2_interim(j-1) chrom2_interim(j:end)];
    end
    chrom2a=cat(2,chrom2a,chrom2_interim(j));
end
%%
waitbar(0.9,h,'Almost done...');
close(h);
% figure;
% subplot(2,1,1)
% plot(t1,chrom1,'--b')
% hold on
% plot(t2,chrom2,'-.r')
% hold off
% title('Before alignment')
% 
% subplot(2,1,2)
% if numel(chrom1a)~=numel(t1_new)
%     plot(chrom1a,'--b')
%     hold on
%     plot(chrom2a,'-.r')
%     hold off
% else
%     plot(t1_new,chrom1a,'--b')
%     hold on
%     plot(t1_new,chrom2a,'-.r')
%     hold off
% end
% title('After alignment')

% subplot(2,2,3)
% plot(t1,chrom1,'--b')
% hold on
% for i=1:numel(Peaks_std)
%     x = [Peaks_std(i).interval(1) Peaks_std(i).interval(2)];
%     y = x./x*0;
%     line(x,y,'LineStyle','-','LineWidth',4);
% end
% hold off
% 
% subplot(2,2,4)
% plot(t2,chrom2,'-.r')
% hold on
% for i=1:numel(Peaks_std)
%     x = [Peaks(i).interval(1) Peaks(i).interval(2)];
%     y = x./x*0;
%     line(x,y,'LineStyle','-','LineWidth',4);
% end
% hold off