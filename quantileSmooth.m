function [baseline]=quantileSmooth(t,chrom, doPlot)
    if nargin<3
        doPlot=0;
    end
    %extrapolate to earlier times; this removes edge effects that are present
    %at new times
    tMin=min(t);
    dt=mean(diff(t));
    tExtend=fliplr(tMin-dt:-dt:tMin-1);
    chromSmooth=smooth(chrom-chrom(1), 100)+chrom(1);
    chromExtend=interp1(t,chromSmooth,tExtend, 'linear', 'extrap');
    tExtended=[tExtend';t];
    chromExtended=[chromExtend'; chrom];
    
    
    
    
    smFac=.1;
    smOrd=2;
    domainRange=1000;
    domainPercentile=.02;
    domain=ones(domainRange,1);
    nIt=200;
    baseline=0;
    currChrom=chromExtended;
    weights=ones(size(tExtended));
    
    for i=1:nIt
        chromOrdFilt=ordfilt2(currChrom, round(domainPercentile*domainRange), domain, 'symmetric');
        currBaseline=whitsmddw(tExtended,chromOrdFilt,weights, smFac,smOrd);
        currChrom=currChrom-currBaseline;
        baseline=baseline+currBaseline;
        
        
        
        if doPlot
        plot(tExtended, chromExtended);
        hold on
        plot(tExtended, baseline,'r');
        hold off
        pause(.01)
        end
    end
    baseline=baseline(numel(tExtend)+1:end);
