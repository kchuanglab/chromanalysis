   function Peaks = peak_label(chrom,Pint,Peaks,search)
% % %
%  clear
% clc
% close all
% load('Standard_Peaks.mat')
% load('Standard_MG1655_071812.mat')
% 
% % load('Peaks_p308 Ec mrdA K12 KO 061512.mat')
% % load('p308 Ec mrdA K12 KO 061512.mat')
% % chrom = chrom2;
% for i = 1: size(Peaks,2)
%     Pname{i} = Peaks(i).name; 
%     Pint(i,1) = Peaks(i).position;
%     Pint(i,2:3) = Peaks(i).interval;
% end

chrom.coeff2(chrom.coeff2(:,3)<0,:)=[];
PData = sortrows(chrom.coeff2,3);
PData(:,4)=0; %flag that the peak has not been detected
baseline = median(PData(:,1))/5;
%%
for p = 1:size(Pint,1)
    assigned_peaks = [];
    size_int = (Pint(p,2)-Pint(p,1))/2;
    move = 0;
    cont_search = 1;
    % look around within the peak interval to find a peak
    while isempty(assigned_peaks)&&cont_search==1 %search for peaks also beyond the peak area but for no more than half the peak interval
        delta_minus = Pint(p,1)-move/2;
        delta_plus = Pint(p,2)+move/2;
        Peaks(p).interval(1) = Pint(p,1)-move/2;
        Peaks(p).interval(2) = Pint(p,2)+move/2;
        % find peaks greater than the baseline in the region around delta minus
        % and delta plus
        interval = PData(PData(:,3)>delta_minus & PData(:,3)<delta_plus & PData(:,1)>baseline & PData(:,4)==0,:);
        interval(:,4)=find(PData(:,3)>delta_minus&PData(:,3)<delta_plus & PData(:,1)>baseline & PData(:,4)==0);
        
        max_p = interval(interval(:,1)==max(interval(:,1)),:);
        assigned_peaks = interval(:,4);
        if search %do not search if manually labeling peaks, do only when optimizing labeling
            move = move + size_int/10;
            if move>size_int
                cont_search=0;
            end
        else
            cont_search=0;
        end
    end
%     assigned_peaks = interval(interval(:,3)>max_p(1,3)-abs(max_p(1,2)*1.5)&interval(:,3)<max_p(1,3)+abs(max_p(1,2)*1.5),4);
    PData(assigned_peaks,4)=1;
    
    
    Peaks(p).position = PData(assigned_peaks,3);
    Peaks(p).gaussian = PData(assigned_peaks,1:2);
    
    clear interval assigned_peaks
end


%%
% figure;
% 
% plot(chrom.t,chrom.final(:,1))
% hold on
% plot(chrom.coeff2(:,3),chrom.coeff2(:,1),'r.')
% hold off
% for i =1:size(Peaks,2)
%     for j=1:size(Peaks(i).position,1)
%         txthandle = text(Peaks(i).position(j), Peaks(i).gaussian(j,1), Peaks(i).name);
%         set(txthandle, 'rotation', 45,'fontsize',15);
%     end
% end

