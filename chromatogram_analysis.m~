function chrom = chromatogram_analysis(filename,params)
KC_smooth = 0;
h = waitbar(0,'Loading the file...');

% determine whether to pause in between steps
if isfield(params,'pause')
    dopause = params.pause;
else
    dopause = 0;
end

% fitting parameters
opts = statset('MaxIter',600);

% subplot parameters
sp_nx = 2;
sp_ny = 2;
nsp = 0; % number of the subplot

% determine whether to add directory to filename
if isfield(params,'dir')
    if length(params.dir)>0
        filename = [params.dir,'/',filename];
    end
end

%%
%% Step 1: Read data and detect peaks and valleys
%%

chrom.orig_data = dlmread(filename);

if params.subtract_blank
    sl = strfind(filename,'/');
    bm = strfind(params.blank_motif,'*');
    blank_file = [params.dir '/' params.blank_motif(1:bm-1) filename(sl(end)+1:end)];
    chrom.blank = dlmread(blank_file);
    if size(chrom.orig_data) ~= size(chrom.blank)
        errordlg('The sample and blank data must match in size to be subtracted and this is not the case.')
        return
    end
    chrom.blank_sub_data = chrom.orig_data;
    chrom.blank_sub_data(:,2) = chrom.orig_data(:,2) - chrom.blank(:,2);
    %     figure;
    %     plot(chrom.blank_sub_data(:,1),chrom.blank_sub_data(:,2),'LineWidth',1.2);
    %     hold on
    %     plot(chrom.orig_data(:,1),chrom.orig_data(:,2),'r');
    %     hold off
else
    chrom.blank_sub_data = chrom.orig_data;
end

% cut off a certain number of points at either end, due to noise
% and/or random peaks that are unimportant
if isfield(params,'perc');
    perc = params.perc/100;
else
    perc = 0.05;
end

nt = size(chrom.orig_data,1); % number of time points
working_data = chrom.blank_sub_data(max(round(perc*nt),1):round((1-perc)*nt),:);
% identify peaks in the data
[x0,y0,peaks] = findpeaks(working_data(:,1),working_data(:,2));

if KC_smooth
    
    % identify valleys
    [x0,y0,valleys] = findpeaks(working_data(:,1),-working_data(:,2));
    
    % loop over valleys to see which ones have a large jump associated
    % with them
    if isfield(params,'thresh')
        thresh = params.thresh;
    else
        thresh = 0.01;
    end
    
    for j=1:numel(valleys)-1
        if working_data(valleys(j+1),2)-working_data(valleys(j),2)>thresh
            jump(j) = 1;
        else
            jump(j) = 0;
        end
    end
    
    % highlight points where there was a big jump
    mark = zeros(size(valleys));
    for j=1:numel(valleys)-2
        if jump(j)
            mark(j+1) = 1;
        end
    end
    
    %%
    %% Step 2: Detect baseline
    %%
    waitbar(0.1,h,'Detecting the baseline...');
    % interpolate to find the value of the baseline at each peak
    
    % define baseline by the valleys where data has not jumped up too much
    baseline(:,:) = working_data(valleys(mark==0),:);
    % add to baseline the starting and end point to make sure baseline
    % encapsulates all the data
    baseline = [working_data(1,1) working_data(1,2); baseline; working_data(end,1) working_data(end,2)];
    
    % replace biggest jumps in the baseline with an interpolated value
    % of what is in the middle, do this iteratively
    iterrep = 300; % number of iterations
    baseline0 = baseline;
    for k=1:iterrep
        delta = zeros(1,size(baseline0,1)); % sort of the curvature of
        % the baseline
        for j=2:(size(baseline0,1)-1)
            if baseline0(j+1,2)<baseline0(j,2) & baseline0(j-1,2)<baseline0(j,2)
                delta(j) = delta(j)-baseline0(j+1,2)-baseline0(j-1,2)+2*baseline0(j,2);
            end
        end
        
        [sort_delta,ind_sd] = sort(delta);
        
        nrep = 20;
        baseline_new = baseline0;
        for j=1:nrep
            ind = ind_sd(end-j+1);
            
            if sort_delta(end-j+1)>0.0001
                % replace with average of the two
                diff = baseline0(ind+1,2)-baseline0(ind-1,2);
                shift = baseline0(ind+1,1)-baseline0(ind-1,1);
                
                baseline_new(ind,2) = baseline0(ind-1,2)+diff/shift* ...
                    (baseline0(ind,1)-baseline0(ind-1,1));
            end
        end
        baseline0 = baseline_new;
    end
    baseline = baseline0;
    
    % now go through and find valleys in the baseline and then remove
    % the points on either side; do this iteratively
    nq = 3; % number of iterations
    for q=1:nq
        % remove valleys in baseline
        [xv,yv,valleys_baseline] = findpeaks(baseline(:,1),-baseline(:,2));
        mark_baseline = zeros(1,size(baseline,1));
        for q=2:numel(valleys_baseline)-1
            if baseline(valleys_baseline(q)+1,2)>baseline(valleys_baseline(q),2) ...
                    & baseline(valleys_baseline(q)-1,2)>baseline(valleys_baseline(q),2)
                mark_baseline(valleys_baseline(q)+1) = 1;
                mark_baseline(valleys_baseline(q)-1) = 1;
                %fprintf('Marking baselines %d and %d...\n',valleys_baseline(q)-1,valleys_baseline(q)+1);
            end
        end
        fb = find(mark_baseline==0);
        baseline_old = baseline;
        baseline = baseline(fb,:);
    end
    
    % define the value of the baseline at each of the peaks, and across
    % the entire chromatogram, using interpolation
    valpeaks = interp1(baseline(:,1),baseline(:,2),working_data(peaks,1));
    vals = interp1(baseline(:,1),baseline(:,2),working_data(:,1),'linear');
    
    % make sure the baseline is always below the chromatogram
    vals = min(vals,working_data(:,2));
else
    waitbar(0.1,h,'Detecting the baseline...');
    [vals]=quantileSmooth(working_data(:,1),working_data(:,2));
    baseline(:,1)=working_data(:,1);
    baseline(:,2)=vals;
end
% find the largest peaks
working_data_mb = working_data(:,2)-vals; % subtract the baseline!
maxima = working_data_mb(peaks);
[smax,indmax] = sort(maxima);

% define value at the peaks
valpeaks = interp1(working_data(:,1),vals(:,1),working_data(peaks,1));

%%
%% Figure 1: Plot of the data with peaks labeled
%%

% % plot the data
% figure;
% nsp = nsp+1;
% subplot(sp_nx,sp_ny,1);
% 
% plot(working_data(:,1),working_data(:,2),'LineWidth',1.5);
% hold on;
% % scatter(working_data(peaks,1),working_data(peaks,2),50,[1 0.5 0],'filled');
% % scatter(working_data(valleys,1),working_data(valleys,2),50,[0 0.5 1],'filled');
% % scatter(working_data(valleys(mark==1),1),working_data(valleys(mark==1),2),200,[1 0 0],'filled');
% % draw line connecting all the valleys
% % plot(working_data(valleys(mark==0),1),working_data(valleys(mark==0),2),'Color',[0.5 0.5 1],'LineWidth',1.5);
% % scatter(working_data(peaks,1),valpeaks,75,[0 1 0],'filled');
% % plot(baseline(:,1),baseline(:,2),'Color',[0 0 0],'LineWidth',1);
% plot(working_data(:,1),vals(:,1),'Color',[0.5 0.5 0.5],'LineWidth',1.5);
% hold off
% pause
%%
%% Step 3: Fit the peaks
%%
waitbar(0.2,h,'Fitting the peaks...');
% number of points for fitting on either side of peak
if isfield(params,'n')
    n = params.n;
else
    n = 50;
end

% loop over a certain number of peaks
npeaks = 50;
for j=1:npeaks
    % focus on the maximal peak
    p = peaks(indmax(end-j+1));
    
    % fit a Gaussian to the points around this peak
    ind0 = max(1,p-n);
    ind1 = min(p+n,size(working_data,1));
    x = working_data(ind0:ind1,1);
    y = working_data(ind0:ind1,2);
    base = vals(ind0:ind1);
    
    % amplitude and width
    beta0 = [working_data(p,2) 0.05 working_data(p,1)];
    
    % shift y to take into account the expected baseline level
    warning off all;
    [beta,r] = nlinfit(x,y-base,@gaussfit,beta0,opts);
    warning on all;
    
    % figure out residuals
    if numel(r)>numel(beta)
        MSE(j) = norm(r)^2/(numel(r)-numel(beta));
    else
        MSE(j) = NaN;
    end
    
    %     fprintf('Analyzing peak at %g (%g; %d):', ...
    %             working_data(p,1),working_data(p,2),p);
    %
    %     fprintf('\tFit is %6.4f*exp(-(x-%4.2f)^2/2/%4.3f^2).', ...
    %             beta(1),beta(3),beta(2));
    
    % plot the gaussian
    x2 = working_data(ind0:ind1,1);
    %     plot(x2,gaussfit(beta,x2)+base,'Color',[1 0 1],'LineWidth',1.25);
    
    betas(j,:) = beta;
    init_center(j) = working_data(p,1);
    init_amp(j) = working_data(p,2)-vals(p);
    shift_center(j) = abs(init_center(j)-betas(j,3))/(max(working_data(:,1))-min(working_data(:,1)));
    shift_amp(j) = abs(init_amp(j)-betas(j,1))/init_amp(j);
    
    if shift_center(j)>0.02 || shift_amp(j)>0.1 || betas(j,1)<0
        %         fprintf(' SWITCH %g %g %g.\n',shift_center(j),shift_amp(j),betas(j,1));
        betas(j,1) = 0;
        betas(j,2) = 0.1;
        betas(j,3) = beta0(3);
    else
        %         fprintf('\n');
    end
end

% make all sigmas positive
betas(:,2) = abs(betas(:,2));

%%
%% Refit for peaks that are close together
%%
waitbar(0.3,h,'Still fitting the peaks...');
% find peaks that are too close
alpha = 1.5;
[sort_center,ind_center] = sort(betas(:,3));
up = betas(ind_center,3)+betas(ind_center,2)*alpha;
down = betas(ind_center,3)-betas(ind_center,2)*alpha;

% fprintf('\n');
mark_rep = zeros(1,numel(up));
for j=1:numel(up)-1
    if up(j)>down(j+1)
        %         fprintf('Peaks %2d and %2d at %5.3f and %5.3f overlap.\n',j,j+1, ...
        %                 sort_center(j),sort_center(j+1));
        mark_rep(j) = mark_rep(j)+1;
        mark_rep(j+1) = mark_rep(j+1)+1;
    end
end
% fprintf('\n');

% replace betas
f = find(mark_rep>0);
j = 1;
n = 100;
while j<numel(f)
    change = f(j);
    % find next element that is a 1
    q = 1;
    while mark_rep(change+q)==2
        q = q+1;
    end
    
    % fit to gaussians
    f0 = find(working_data(:,1)>=sort_center(change));
    f1 = find(working_data(:,1)<=sort_center(change+q));
    ind0 = max(1,f0(1)-n);
    ind1 = min(f1(end)+n,size(working_data,1));
    x = working_data(ind0:ind1,1);
    y = working_data(ind0:ind1,2);
    base = vals(ind0:ind1);
    beta0 = betas(ind_center(change),:);
    for m=1:q
        %        beta0 = [beta0 betas(ind_center(change+m),:)];
        beta0 = [beta0 betas(ind_center(change+m),1) 0.02 betas(ind_center(change+m),3)];
    end
    warning off all;
    [beta,r] = nlinfit(x,y-base,@gaussfit,beta0,opts);
    warning on all;
    
    % keep track of the coefficients before switching
    beta_start = betas;
    
    for m=0:q
        betas(ind_center(change+m),:) = beta(m*3+1:m*3+3);
    end
    
    % query whether it is better or worse to have the new or old
    % betas
    chrom0 = compute_chrom(working_data(:,1),beta_start);
    chrom1 = compute_chrom(working_data(:,1),betas);
    
    div0 = measure(chrom0.data',(working_data(:,2)-vals)',0);
    div1 = measure(chrom1.data',(working_data(:,2)-vals)',0);
    
    %     fprintf('Fitting %2d gaussians to peaks %2d to %2d...',q+1,change,change+q);
    
    if div0>div1
        str = 'YES';
    else
        str = 'NO';
    end
    
    %     fprintf('\tDivergence before/after %6.4f %6.4f: %s\n',div0,div1,str);
    
    if div0<div1
        betas = beta_start;
        %fprintf('Switching back to the initial coefficients. %d %d\n',j,q)
    else
        % warn if any of the betas have negative amplitude
        fltz = find(beta(1:3:end)<0);
        if numel(fltz)>0
            % recalculate divergence
            betanew = betas;
            
            %             fprintf('WARNING: coefficient<0 -');
            for r=1:numel(fltz)
                %                 fprintf(' (%d) %6.4f',fltz(r),beta(fltz(r)*3-2));
                betanew(ind_center(change+fltz(r)-1),1) = 0;
            end
            fprintf('\n');
            
            chromnew = compute_chrom(working_data(:,1),betanew);
            divnew = measure(chromnew.data',(working_data(:,2)-vals)',0);
            if divnew>div0
                % go back to original data
                betas = beta_start;
            else
                betas = betanew;
                if divnew>div1
                    %                     fprintf('Warning: %6.4f %6.4f -> %6.4f\n',div0,div1,divnew);
                end
            end
        end
    end
    
    j = j+q+1;
end
waitbar(0.5,h,'Still fitting the peaks...');
%output_coefficients(betas,0);

% maximum mean-squared error
maxmse = 3*mean(MSE);

% sum over all gaussians
curve = zeros(size(working_data(:,1)));
for j=1:size(betas,1)
    %%    if ~isnan(MSE(j)) & MSE(j)<maxmse
    %    if shift_center(j)<median(shift_center)*3 || shift_amp(j)<0.1
    curve = curve+gaussfit(betas(j,:),working_data(:,1));
    %else
    %betas(j,1) = 0;
    %betas(j,2) = 0.1;
    %end
end
curve = curve+vals;
% plot(working_data(:,1),curve,'Color',[0 1 1],'LineWidth',2);
valpeaks = interp1(working_data(:,1),vals(:,1),betas(:,3));
% scatter(betas(:,3),betas(:,1)+valpeaks,40,[0 1 0],'filled');
hold off;

[sort_beta,ind_beta] = sort(betas(:,3));

% title('Initial processing');
% xlabel('Time (ms)');
% ylabel('Absorption');
%
% ax = axis;
% ax(3) = -0.2;
% axis(ax);

if dopause
    pause
end

%%
%% Figure 2
%%

% fprintf('\nSTEP 2: ANALYZE THE DIFFERENCE...\n\n');

% plot the difference, figure 2
ch2(:,1) = working_data(:,1);
ch2(:,2) = working_data(:,2)-curve;

% now pinpoint the peaks in the new curve
[x2,y2,peaks2] = findpeaks(ch2(:,1),ch2(:,2));

% identify valleys
[x2,y2,valleys2] = findpeaks(ch2(:,1),max(ch2(:,2))-ch2(:,2));

% find maxima at each peak values
maxima2 = ch2(peaks2,2);
[smax2,indmax2] = sort(maxima2);

% plot the difference between the current curve and the end curve
nsp = nsp+1;
% subplot(sp_nx,sp_ny,nsp);

% plot(working_data(:,1),working_data(:,2)-curve);
% hold on;
% scatter(ch2(peaks2,1),ch2(peaks2,2),50,[1 0.5 0],'filled');
% scatter(ch2(valleys2,1),ch2(valleys2,2),50,[0 0.5 1],'filled');

% fprintf('\nSTEP 3: SECOND PASS ANALYSIS...\n\n');

% loop over a certain number of peaks
npeaks2 = 50;
n = 20;
for j=1:npeaks2
    % focus on the maximal peak
    p = peaks2(indmax2(end-j+1));
    
    % fit a Gaussian to the points around this peak
    ind0 = max(1,p-n);
    ind1 = min(p+n,size(ch2,1));
    x = ch2(ind0:ind1,1);
    y = ch2(ind0:ind1,2);
    
    % amplitude and width
    beta0 = [ch2(p,2) 0.05 ch2(p,1)];
    
    % shift y to take into account the expected baseline level
    
    warning off all;
    [beta,r] = nlinfit(x,y,@gaussfit,beta0,opts);
    warning on all;
    
    % figure out residuals
    if numel(r)>numel(beta)
        MSE2(j) = norm(r)^2/(numel(r)-numel(beta));
    else
        MSE2(j) = NaN;
    end
    %
    %     fprintf('Analyzing peak at %g (%g; %d):', ...
    %             ch2(p,1),ch2(p,2),p);
    %
    %     fprintf('\tFit is %6.4f*exp(-(x-%4.2f)^2/2/%4.3f^2).', ...
    %             beta(1),beta(3),beta(2));
    
    % plot the gaussian
    x2 = ch2(ind0:ind1,1);
    %     plot(x2,gaussfit(beta,x2),'Color',[1 0 1],'LineWidth',1.25);
    
    betas2(j,:) = beta;
    init_center2(j) = ch2(p,1);
    init_amp2(j) = ch2(p,2);
    shift_amp2(j) = abs(init_amp2(j)-betas2(j,1))/init_amp2(j);
    shift_center2(j) = abs(init_center2(j)-betas2(j,3))/(max(ch2(:,1))-min(ch2(:,1)));
    
    if shift_center2(j)>0.02 || betas2(j,1)<0
        betas2(j,1) = 0;
        betas2(j,2) = 0.1;
        betas2(j,3) = beta0(3);
        %         fprintf(' SWITCH.\n');
    else
        %         fprintf('\n');
    end
end
% reset sigmas to be positive
betas2(:,2) = abs(betas2(:,2));

% sum over all gaussians
curve2 = zeros(size(ch2(:,1)));
for j=1:size(betas2,1)
    %%    if ~isnan(MSE(j)) & MSE(j)<maxmse
    % compare to *shift_center* median
    if shift_center2(j)<median(shift_center)*3 && abs(betas2(j,2))< ...
            median(abs(betas2(:,2)))*5
        curve2 = curve2+gaussfit(betas2(j,:),ch2(:,1));
    else
        betas2(j,1) = 0;
        betas2(j,2) = 0.1;
    end
end
%plot(ch2(:,1),curve2,'Color',[0 1 1],'LineWidth',1);
%hold off;

% find all peaks that are too close
alpha = 1.5; % changed from alpha=2 on 9/25
betaall = [betas;betas2];
[sort_beta,ind_beta] = sort(betaall(:,3));
%disp(betaall(ind_beta,:));

% screen out all betas that have a negative sigma
%fb = find(betaall(:,2)<0);
%fb2 = find(betaall(:,2)>0);
%disp(betaall(fb,:));

betaall1 = betaall;
%betaall1(fb,2) = 0.1;
%betaall1(fb,1) = 0;

%betaall = betaall(fb2,:);

[sort_center,ind_center] = sort(betaall(:,3));
up = betaall(ind_center,3)+betaall(ind_center,2)*alpha;
down = betaall(ind_center,3)-betaall(ind_center,2)*alpha;

mark_rep = zeros(1,numel(up));
% fprintf('\n');
for j=1:numel(up)-2
    if up(j)>down(j+1) || up(j)>down(j+2)
        %         fprintf('Peaks %2d and %2d at %5.3f and %5.3f overlap.\n',j,j+1, ...
        %                 sort_center(j),sort_center(j+1));
        mark_rep(j) = mark_rep(j)+1;
        mark_rep(j+1) = mark_rep(j+1)+1;
    end
end
if up(end-1)>down(end)
    mark_rep(end-1) = mark_rep(end-1)+1;
end
% fprintf('\n');

% replace betas
f = find(mark_rep>0);
fr = find(mark_rep==0);
j = 1;
n = 100;
close(h)
h=waitbar(0.6,'Almost done fitting the peaks...');
while j<numel(f)
    waitbar(0.6+0.3*j/numel(f),h,'Almost done fitting the peaks...');
    change = f(j);
    % find next element that is a 1
    q = 1;
    while mark_rep(change+q)==2
        q = q+1;
    end
    
    % fit to gaussians
    f0 = find(working_data(:,1)>=sort_center(change));
    f1 = find(working_data(:,1)<=sort_center(change+q));
    ind0 = max(1,f0(1)-n);
    ind1 = min(f1(end)+n,size(working_data,1));
    x = working_data(ind0:ind1,1);
    y = working_data(ind0:ind1,2);
    base = vals(ind0:ind1);
    beta0 = betaall(ind_center(change),:);
    for m=1:q
        beta0 = [beta0 betaall(ind_center(change+m),:)];
    end
    warning off all;
    [beta,r] = nlinfit(x,y-base,@gaussfit,beta0,opts);
    warning on all;
    
    % keep track of the coefficients before switching
    beta_start = betaall;
    
    for m=0:q
        betaall(ind_center(change+m),:) = beta(m*3+1:m*3+3);
    end
    
    % query whether it is better or worse to have the new or old
    % betaall
    chrom0 = compute_chrom(working_data(:,1),beta_start);
    chrom1 = compute_chrom(working_data(:,1),betaall);
    
    %     fprintf('Fitting %2d gaussians to peaks %2d to %2d...',q+1,change,change+q);
    
    div0 = measure(chrom0.data',(working_data(:,2)-vals)',0);
    div1 = measure(chrom1.data',(working_data(:,2)-vals)',0);
    
    if div0>div1
        str = 'YES';
    else
        str = 'NO';
    end
    
    %     fprintf('\tDivergence before/after %6.4f %6.4f: %s\n',div0,div1,str);
    
    if div0<div1
        betaall = beta_start;
        %fprintf('Switching back to the initial coefficients. %d %d\n',j,q)
    else
        % warn if any of the betas have negative amplitude
        fltz = find(beta(1:3:end)<0);
        if numel(fltz)>0
            % recalculate divergence
            betanew = betaall;
            
            %             fprintf('WARNING: coefficient<0 -');
            for r=1:numel(fltz)
                %                 fprintf(' (%d) %6.4f',fltz(r),beta(fltz(r)*3-2));
                betanew(ind_center(change+fltz(r)-1),1) = 0;
            end
            %             fprintf('\n');
            
            chromnew = compute_chrom(working_data(:,1),betanew);
            divnew = measure(chromnew.data',(working_data(:,2)-vals)',0);
            if divnew>div0
                % go back to original data
                betaall = beta_start;
            else
                betaall = betanew;
                if divnew>div1
                    %                     fprintf('Warning: %6.4f %6.4f -> %6.4f\n',div0,div1,divnew);
                end
            end
        end
    end
    
    j = j+q+1;
end

if dopause
    pause
end

% plot(ch2(:,1),curve2,'Color',[0 1 1],'LineWidth',2);
% hold off;
% title('Error after initial processing');
% xlabel('Time (ms)');
% ylabel('Absorption');
%%
%% Figure 3: New fit
%%
waitbar(0.9,h,'Almost done fitting the peaks...');
% make new figure with new fit, figure 3
%figure;
nsp = nsp+1;
% subplot(sp_nx,sp_ny,nsp);
% plot(working_data(:,1),working_data(:,2),'Color',[0 0 1],'LineWidth',2);
% hold on;
% plot(working_data(:,1),curve+curve2,'Color',[0 1 1],'LineWidth',1.5);
% plot(working_data(:,1),vals,'Color',[0.5 0.5 0.5]);
%ch_test = compute_chrom(working_data(:,1),betaall1);
%plot(working_data(:,1),vals+ch_test.data,'Color',[1 0 0],'LineWidth',3);

% sum over all gaussians with new betas
curveall = zeros(size(ch2(:,1)));
for j=1:size(betaall,1)
    if betaall(j,1)>0
        % find point closest to center
        f0 = find(working_data(:,1)>betaall(j,3));
        if numel(f0)>0 && betaall(j,1)<(working_data(f0(1),2)-vals(f0(1)))*1.1
            curveall = curveall+gaussfit(betaall(j,:),ch2(:,1));
        else
            betaall(j,1) = 0;
            betaall(j,2) = 0.1;
        end
    end
end
% plot(working_data(:,1),curveall+vals,'Color',[1 0 1],'LineWidth',2);
fz = find(betaall(:,3)>0);
% associate baseline with all values
valgauss = interp1(baseline(:,1),baseline(:,2),betaall(fz,3));
% scatter(betaall(fz,3),betaall(fz,1)+valgauss,40,[0 1 0],'filled');
% hold off;
% title('Chromatogram including second round of peak detection');
% xlabel('Time (ms)');
% ylabel('Absorption');
% legend('Original','1st+2nd','Redistributed');
ax = axis;
ax(3) = -0.2;
axis(ax);

if dopause
    pause
end

%%
%% Compile data
chrom.working_with_baseline = working_data(:,2);
chrom.working = working_data(:,2)-vals;
chrom.final_with_baseline = curveall+vals;
chrom.final = curveall;
chrom.coeff1 = betas;
chrom.coeff2 = betaall;
chrom.t = working_data(:,1);



%%
%% Figure 4: remaining error
%%
nsp = nsp+1;
% subplot(sp_nx,sp_ny,nsp);
% plot(working_data(:,1),working_data(:,2)-chrom.final_with_baseline, ...
%      'Color',[0 0 1],'LineWidth',1.5);
% hold on;
% plot(working_data(:,1),abs(working_data(:,2)-chrom.final_with_baseline), ...
%      'Color',[0 1 1],'LineWidth',1);
% hold off;
% title('Error after second processing');
% xlabel('Time (ms)');
% ylabel('Absorption');
%%
chrom.coeff2(chrom.coeff2(:,3)<0,:)=[];
chrom.coeff2(chrom.coeff2(:,3)>60,:)=[];

% ks1=strfind(filename,'/');
% ks=strfind(filename,'.arw');
% out_save = filename(max(ks)+1:ks-1);
% save([out_save '.mat'],'chrom');
waitbar(1,h,'All done!');
close(h)

