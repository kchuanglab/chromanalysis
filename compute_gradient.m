function [gradA,grads,gradm] = compute_gradient(actual,betas,shift);

db = 10^-4;
for q=1:size(betas,1)
    betas(q,1) = betas(q,1)+db;
    pred = compute_chrom(actual(:,1),betas);
    divp = measure(pred.data',actual(:,2)',shift);
    
    betas(q,1) = betas(q,1)-2*db;
    pred = compute_chrom(actual(:,1),betas);
    divm = measure(pred.data',actual(:,2)',shift);
    
    betas(q,1) = betas(q,1)+db;    
    
    gradA(q) = (divp-divm)/(2*db);
    
    betas(q,2) = betas(q,2)+db;
    pred = compute_chrom(actual(:,1),betas);
    divp = measure(pred.data',actual(:,2)',shift);
    
    betas(q,2) = betas(q,2)-2*db;
    pred = compute_chrom(actual(:,1),betas);
    divm = measure(pred.data',actual(:,2)',shift);
    
    betas(q,2) = betas(q,2)+db;    
    
    grads(q) = (divp-divm)/(2*db);
    
    betas(q,3) = betas(q,3)+db;
    pred = compute_chrom(actual(:,1),betas);
    divp = measure(pred.data',actual(:,2)',shift);
    
    betas(q,3) = betas(q,3)-2*db;
    pred = compute_chrom(actual(:,1),betas);
    divm = measure(pred.data',actual(:,2)',shift);
    
    betas(q,3) = betas(q,3)+db;    
    
    gradm(q) = (divp-divm)/(2*db);
end
