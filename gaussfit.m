% sum of Gaussians! %Lorentzians!
function y = gaussfit(beta,x)

% lorentzian is A/((x-x0)^2+B^2)
y = zeros(size(x));
for i=1:3:numel(beta)
    y = y + beta(i)*exp(-(x-beta(i+2)).^2/2/beta(i+1)^2);
    %y = y + beta(i)*beta(i+1)^2./((x-beta(i+2)).^2 + beta(i+1)^2);
end
