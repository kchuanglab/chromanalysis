%% Analyze and plot calculated peaks
clear
clc
close all
folder_name = uigetdir(cd,'Select directory of .mat files to be compiled');
cd (folder_name)

Species = struct('file',[],'name',[],'peak',[],'monomer',[],'dimer',[],'trimer',[],'lipoprotein',[],'anhydro',[],'dapdap',[],'pentapepeptide',[],'pentaglycine',[]);
kk = 1;
Species(kk).name = 'MG1655';
kk = kk+1;
%  Species(kk).name = '0p01upm_Mec';
%  kk = kk+1;
% % Species(kk).name = '0p1upm_Mec';
% % kk = kk+1;
% % Species(kk).name = 'a1upm_Mec';
% % kk = kk+1;
% % Species(kk).name = '20upm_Mec';
% % kk = kk+1;
Species(kk).name = 'Ec';
kk = kk+1;
Species(kk).name = 'St';
kk = kk+1;
Species(kk).name = 'Yp';
kk = kk+1;
Species(kk).name = 'Vc';
kk = kk+1;
% Species(kk).name = 'A53T';
% kk = kk+1;
% Species(kk).name = 'Ec_oe';
% kk = kk+1;
% Species(kk).name = 'Ec_ue';%Ec pBAD low [PBP2]
% kk = kk+1;
% Species(kk).name = 'PBP2_transpep_mut';
% kk = kk+1;
%%
for nin =1:size(Species,2)
    A=dir(fullfile(folder_name, ['*' Species(nin).name '*.mat']));
    for n=1:size(A)
        load(A(n).name)
        for i=1:size(Peaks,2)
            Species(nin).peak(i).file(n,:) = {A(n).name};
            Species(nin).peak(i).area(n,1) = Peaks(i).area;
            Species(nin).peak(i).name = Peaks(i).name;
        end
        [Species(nin).monomer(n),Species(nin).dimer(n),Species(nin).trimer(n),Species(nin).lipoprotein(n),Species(nin).anhydro(n),Species(nin).dapdap(n),Species(nin).pentapepeptide(n),Species(nin).pentaglycine(n)] = muropeptide_calc(Peaks);
        Species(nin).crosslinked(n) = Species(nin).dimer(n) + 2* Species(nin).trimer(n);
        Species(nin).glylength(n) = 1 / Species(nin).anhydro(n);
    end
    clearvars -except Species nn k nin folder_name
end

%% Plot all the peaks
data = zeros(numel(Species(1).peak),size(Species,2));
errdata = zeros(numel(Species(1).peak),size(Species,2));
name_peaks = [];
formatSpec = '%s';
formatSpec_row1 = '%s';
row1{1}='';
for peak=1:numel(Species(1).peak)
    for spec=1:size(Species,2)
        data(peak,spec)=mean(Species(spec).peak(peak).area);
        errdata(peak,spec)=std(Species(spec).peak(peak).area);
        row1{2*spec} = [Species(spec).name];
        row1{2*spec+1} ='Std';
        xlsdata{peak,2*spec-1}=data(peak,spec);
        xlsdata{peak,2*spec}=errdata(peak,spec);
        if peak==1
            formatSpec = [formatSpec ' %12.8f %12.8f'];
            formatSpec_row1 = [formatSpec_row1 ' %s %s'];
        end
    end
    name_peaks = strcat(name_peaks,['|' Species(spec).peak(peak).name]);
    col1{peak} = Species(spec).peak(peak).name;
end
formatSpec = [formatSpec '\n'];
formatSpec_row1 = [formatSpec_row1 '\n'];
% xlsdata = cat(1, row1,xlsdata);
xlsdata = cat(2, col1',xlsdata);
filename = ['Compile_data_' date '.txt'];
fileID = fopen(filename,'w');
fprintf(fileID,formatSpec_row1,row1{:});
[nrows,ncols] = size(xlsdata);
for row = 1:nrows
    fprintf(fileID,formatSpec,xlsdata{row,:});
end

plot_black =0;
scrsz = get(0,'ScreenSize');
for f=1:2
    figure(f);
    set(gcf,'Position',[scrsz(4)*0.25 scrsz(4) scrsz(4)*1.5 0.75*scrsz(4)]);
    h = bar(data);
    colormap(hsv)
    
    set(gca,'GridLineStyle','-')
    set(get(gca,'YLabel'),'String','')
    set(gca,'XLim',[0 numel(Species(1).peak)])
    set(gca,'XTick',0:1:numel(Species(1).peak))
    set(gca,'XTicklabel',name_peaks)
    lh(f) = legend({Species.name});
    set(lh(f),'Location','BestOutside','Orientation','horizontal','Interpreter','none')
    hold on;
    numgroups = size(data, 1);
    numbars = size(data, 2);
    groupwidth = min(0.8, numbars/(numbars+1.5));
    for i = 1:numbars
        % Based on barweb.m by Bolu Ajiboye from MATLAB File Exchange
        x = (1:numgroups) - groupwidth/2 + (2*i-1) * groupwidth / (2*numbars); % Aligning error bar with individual bar
        if plot_black
            errorbar(x, data(:,i), errdata(:,i), 'k', 'linestyle', 'none','Color',[1 1 1]);
        else
            errorbar(x, data(:,i), errdata(:,i), 'k', 'linestyle', 'none');
        end
    end
    fontsize = 20;
    if plot_black
        set(gcf,'color','k');
        set(gca,'Xcolor','w');
        set(gca,'Ycolor','w');
        set(gca,'FontSize',fontsize-2)
        set(get(gca,'XLabel'),'FontSize',fontsize,'color','w')
        set(get(gca,'YLabel'),'FontSize',fontsize,'color','w')
        set(get(gca,'Title'),'FontSize',fontsize-2,'color','w')
        set(gca,'color','k')
        set(lh(f),'Interpreter','none','TextColor',[1 1 1],'EdgeColor',[1 1 1],...
            'Orientation','horizontal','Color','k',...
            'Location','NorthOutside',...
            'YColor',[1 1 1],...
            'XColor',[1 1 1]);
        set(findobj('type', 'text'), 'color', 'white');
    end
end
figure(1)
axis([0.5 12.5 0 0.4])
set(gcf,'PaperPositionMode','auto');set(gcf,'InvertHardcopy','off');
% print('-depsc','-tiff','-r300','Summary_peaks_Mec_expr1')
figure(2)
axis([12.5 25.5 0 0.085])
set(gcf,'PaperPositionMode','auto');set(gcf,'InvertHardcopy','off');
% print('-depsc','-tiff','-r300','Summary_peaks_Mec_expr2')


%% Plot the standard values
result = zeros(10,size(Species,2));
err_result = zeros(10,size(Species,2));
name_peaks1 = 'strandlength|crosslink|monomer|dimer|trimer|lipoprotein|anhydro|dap-dap|pentapepeptide|pentaglycine';
col1a={'strandlength';'crosslink';'monomer';'dimer';'trimer';'lipoprotein';'anhydro';'dap-dap';'pentapepeptide';'pentaglycine'};
for spec =1:size(Species,2)
    result(1,spec)=mean(Species(spec).glylength/100);
    err_result(1,spec)=std(Species(spec).glylength/100);
    result(2,spec)=mean(Species(spec).crosslinked);
    err_result(2,spec)=std(Species(spec).crosslinked);
    result(3,spec)=mean(Species(spec).monomer);
    err_result(3,spec)=std(Species(spec).monomer);
    result(4,spec)=mean(Species(spec).dimer);
    err_result(4,spec)=std(Species(spec).dimer);
    result(5,spec)=mean(Species(spec).trimer);
    err_result(5,spec)=std(Species(spec).trimer);
    result(6,spec)=mean(Species(spec).lipoprotein);
    err_result(6,spec)=std(Species(spec).lipoprotein);
    result(7,spec)=mean(Species(spec).anhydro);
    err_result(7,spec)=std(Species(spec).anhydro);
    result(8,spec)=mean(Species(spec).dapdap);
    err_result(8,spec)=std(Species(spec).dapdap);
    result(9,spec)=mean(Species(spec).pentapepeptide);
    err_result(9,spec)=std(Species(spec).pentapepeptide);
    result(10,spec)=mean(Species(spec).pentaglycine);
    err_result(10,spec)=std(Species(spec).pentaglycine);
    for it =1:size(result,1)
        row1a{2*spec-1} = [ 'Mean ' Species(spec).name];
        row1a{2*spec} = [ 'Std ' Species(spec).name];
        xlsdataa{it,2*spec-1}=result(it,spec);
        xlsdataa{it,2*spec}=errdata(it,spec);
    end
end

% xlsdataa = cat(1,row1a,xlsdataa);
xlsdataa = cat(2,col1a,xlsdataa);
% sheet = 2;
% xlwrite(filename,xlsdataa,sheet)
[nrows,ncols] = size(xlsdataa);
for row = 1:nrows
    fprintf(fileID,formatSpec,xlsdataa{row,:});
end
fclose(fileID);
%%
result = result*100;
err_result = err_result*100;
figure;
set(gcf,'Position',[scrsz(4)*0.25 scrsz(4) scrsz(4)*1.5 0.75*scrsz(4)]);
h1 = bar(result);
colormap(hsv)
set(gca,'GridLineStyle','-')
set(get(gca,'YLabel'),'String','')
set(gca,'XLim',[0 size(result,1)])
set(gca,'XTick',1:size(result,1))
set(gca,'XTicklabel',name_peaks1)
lh1 = legend({Species.name});
set(lh1,'Location','BestOutside','Orientation','horizontal','Interpreter','none')
hold on;
numgroups1 = size(result, 1);
numbars1 = size(result, 2);
groupwidth1 = min(0.8, numbars1/(numbars1+1.5));
for i = 1:numbars1
    % Based on barweb.m by Bolu Ajiboye from MATLAB File Exchange
    x = (1:numgroups1) - groupwidth1/2 + (2*i-1) * groupwidth1 / (2*numbars1); % Aligning error bar with individual bar
    if plot_black
        errorbar(x, result(:,i), err_result(:,i), 'k', 'LineStyle','none','Color',[1 1 1]);
    else
        errorbar(x, result(:,i), err_result(:,i), 'k', 'LineStyle','none');
    end
end
%

if plot_black
    set(gcf,'color','k');
    set(gca,'Xcolor','w');
    set(gca,'Ycolor','w');
    set(gca,'FontSize',fontsize-2)
    set(get(gca,'XLabel'),'FontSize',fontsize,'color','w')
    set(get(gca,'YLabel'),'FontSize',fontsize,'color','w')
    set(get(gca,'Title'),'FontSize',fontsize-2,'color','w')
    set(gca,'color','k')
    set(lh1,'Interpreter','none','TextColor',[1 1 1],'EdgeColor',[1 1 1],...
        'Orientation','horizontal','Color','k',...
        'Location','NorthOutside',...
        'YColor',[1 1 1],...
        'XColor',[1 1 1]);
end
axis([0.5 10.5 0 80])
set(gcf,'PaperPositionMode','auto');
set(gcf,'InvertHardcopy','off');
% set(findobj('type', 'text'), 'color', 'white');
% print('-depsc','-tiff','-r300','Summary_results_Mec_expr')