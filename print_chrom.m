function print_chrom(chrom)

fprintf('mu\tA\tsigma\tArea\n');
for i=1:size(chrom.betas,1)
    fprintf('%6.4f\t%6.4f\t%6.4f\t%6.4f\n',chrom.centers(i),chrom.amps(i), ...
            chrom.sigmas(i),chrom.areas(i));
end
fprintf('\nTotal area = %g\n',chrom.total_area);