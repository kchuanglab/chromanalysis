% relax the predicted chromatogram back to real chromatogram
actual(:,1) = ch1(:,1);
actual(:,2) = ch1(:,2)-vals;

% predicted
betas = betaall;
%betas = betastr{13};
pred = compute_chrom(ch1(:,1),betas);
pred0 = pred;

% calculate kl div
count = 0;
for expshift=-11:-1
    count = count+1;
    shift(count) = 10^expshift;
    div(count) = measure(pred.data',actual(:,2)',shift(count));

    fprintf('The KL divergence is %g for shift=%g\n',div(count),shift(count));
end

shiftr = 10^-8;

niter = 100;
for i=1:niter % cycle over steepest descent
    % cycle over all values in beta
    [gradA,grads,gradm] = compute_gradient(actual,betas,shiftr);
    
% $$$ db = 10^-4;
% $$$ div0 = measure(pred',actual(:,2)',shiftr);
% $$$ for q=1:size(betas,1)
% $$$     betas(q,1) = betas(q,1)+db;
% $$$     pred = compute_chrom(ch1(:,1),betas);
% $$$     divp = measure(pred',actual(:,2)',shiftr);
% $$$     
% $$$     betas(q,1) = betas(q,1)-2*db;
% $$$     pred = compute_chrom(ch1(:,1),betas);
% $$$     divm = measure(pred',actual(:,2)',shiftr);
% $$$     
% $$$     betas(q,1) = betas(q,1)+db;    
% $$$     
% $$$     gradA(q) = (divp-divm)/(2*db);
% $$$ end

    alphaA = 0.00001;
    alphas = 0.00001;    
    alpham = 0.00001;
    betas(:,1) = betas(:,1)-gradA'*alphaA;
    betas(:,2) = betas(:,2)-grads'*alphas;
    betas(:,3) = betas(:,3)-gradm'*alpham;    
    pred = compute_chrom(actual(:,1),betas);
    div = measure(pred.data',actual(:,2)',shiftr);

    maxA = max(abs(gradA));
    maxs = max(abs(grads));
    maxm = max(abs(gradm));
    
    fprintf('%d: div = %g (max=%5.3f %5.3f %5.3f)\n',i,div,maxA,maxs,maxm);
    
    betastr{i} = betas;
end

betas = betasave; %% TAKE OUT

% add gaussians
final = compute_chrom(actual(:,1),betas);
diff = actual(:,2)-final.data;
ns = 300;
sm_abs_diff = smooth(smooth(abs(diff),ns),ns);

% identify peaks
[x0,y0,peaks] = findpeaks(actual(:,1),sm_abs_diff);

[sort_peaks,ind_peaks] = sort(sm_abs_diff(peaks));
add_peaks = min(20,numel(sort_peaks));
mean_sigma = mean(betas(:,2));
for q=1:add_peaks
    p = peaks(ind_peaks(end-q+1));
    i = ind_peaks(end-q+1);
    new_sigma = mean_sigma;
    new_mu = actual(p,1);
    new_amp = sm_abs_diff(p);
    
    betas(end+1,:) = [new_amp new_sigma new_mu];
    
    fprintf('Adding new peak at %5.3f with amp,sigma = %g,%g\n',new_mu,new_amp,new_sigma);
end

niter = 1000;
figure;
for i=1:niter % cycle over steepest descent
    % cycle over all values in beta
    [gradA,grads,gradm] = compute_gradient(actual,betas,shiftr);

    alphaA = 0.001;
    alphas = 0.0003;    
    alpham = 0.0003;
    betas(:,1) = betas(:,1)-gradA'*alphaA;
    betas(:,2) = betas(:,2)-grads'*alphas;
    betas(:,3) = betas(:,3)-gradm'*alpham;    
    pred = compute_chrom(actual(:,1),betas);
    div = measure(pred.data',actual(:,2)',shiftr);

    maxA = max(abs(gradA));
    maxs = max(abs(grads));
    maxm = max(abs(gradm));
    
    fprintf('%d: div = %g (max=%5.3f %5.3f %5.3f)\n',i,div,maxA,maxs,maxm);
    
    betastr{i} = betas;
    
    % make plot
    subplot(1,3,1);
    plot(actual(:,1),actual(:,2),'b','LineWidth',1);
    hold on;
    plot(actual(:,1),pred.data,'r','LineWidth',1);    
    scatter(betas(end-add_peaks+1:end,3),ones(1,add_peaks)*mean(actual(:,2)),[],[0 1 0],'filled');
    hold off;
    
    subplot(1,3,2);
    plot(actual(:,1),actual(:,2)-pred0.data,'b','LineWidth',1);
    hold on;
    plot(actual(:,1),actual(:,2)-pred.data,'r','LineWidth',1);    
    scatter(betas(end-add_peaks+1:end,3),zeros(1,add_peaks),[],[0 1 0],'filled');
    hold off;
    
    subplot(1,3,3);
    plot(actual(:,1),(actual(:,2)-pred0.data)./actual(:,2),'b','LineWidth',1);
    hold on;
    plot(actual(:,1),(actual(:,2)-pred.data)./actual(:,2),'r','LineWidth',1);    
    scatter(betas(end-add_peaks+1:end,3),zeros(1,add_peaks),[],[0 1 0],'filled');
    hold off;
    pause(0.001)
end
