function varargout = findpeaks(x,y,varargin)
start=1;
points=length(y);

%peak finding vectors
lpts=y(start:end-start-2);
mpts=y(start+1:end-start-1);
rpts=y(start+2:end-start);

%find peaks
Rpeaks=zeros(1,points);
Lpeaks=zeros(1,points);
minpeaks=zeros(1,points);
fpeaks=zeros(1,points);
Rpeaks(start+1:end-start-1)=((mpts-lpts)>=0).*((rpts-mpts)<0); %triggers right of peak
Lpeaks(start+1:end-start-1)=((mpts-lpts)>0).*((rpts-mpts)<=0); %triggers left of peak
minpeaks(start+1:end-start-1)=((mpts-lpts)<=0).*((rpts-mpts)>0);

%use minimums and multiple maximums to find best guess of peak
minlist=find(minpeaks==1);

%find and average doubly-occupied peaks
for j=1:length(minlist)+1
    %find bounds between which to look for two pseudo-peaks
    if j==1
        lbound=1;
        if isempty(minlist)
            rbound=points;
        else
            rbound=minlist(j);
        end
    elseif j==length(minlist)+1
        lbound=minlist(j-1);
        rbound=length(y);
    else
        lbound=minlist(j-1);
        rbound=minlist(j);
    end
    
    %look for peaks
    peakmask=zeros(1,points);
    peakmask(lbound:rbound)=1;
    
    Lpeak=find(Lpeaks.*peakmask==1,1);
    Rpeak=find(Rpeaks.*peakmask==1,1);
    fpeaks(round((Lpeak+Rpeak)/2))=1;
end

%check against the peaks in the original data
peakspos=find(fpeaks==1);
npeaks=sum(fpeaks);

varargout(3)={peakspos};
varargout(2)={y};
varargout(1)={x};

return