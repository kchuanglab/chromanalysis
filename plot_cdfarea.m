% plot the CDF for the area
function chrom = plot_cdfarea(chrom)

% calculate the chromatogram based on the coefficients
chrom.c = compute_chrom(chrom.t,chrom.coeff2);

figure;
plot(chrom.c.cdf_area,'Color',[0 0 0],'LineWidth',2);

hold on;
% make lines for the original data
dt = chrom.t(2:end)-chrom.t(1:end-1);

Dt(1) = dt(1);
Dt(2:numel(dt)) = 0.5*(dt(2:end)+dt(1:end-1));
Dt(end+1) = dt(end);

chrom.Dt = Dt;
chrom.orig_area = sum(Dt.*chrom.working');

plot([1 numel(chrom.c.cdf_area)],[chrom.orig_area/chrom.c.total_area ...
                    chrom.orig_area/chrom.c.total_area],'Color',[1 0 0],'LineWidth',2);

% make lines for different values
f = [0.8 0.9 0.95 0.975 0.99];

c0 = [0 0.5 0];
c1 = [0 1 1];
for j=1:numel(f)
    c = c0+(c1-c0)*(j-1)/(numel(f)-1);
    plot([1 numel(chrom.c.cdf_area)],[f(j) f(j)],'Color',c, ...
         'LineWidth',1);
    
    % figure out where it crosses x axis
    fint = find(chrom.c.cdf_area>f(j));
    plot([fint(1) fint(1)],[0 1.05],'-.','Color',c,'LineWidth',1);
end
hold off;
axis([0 numel(chrom.c.cdf_area) 0 1.05]);
xlabel('Number of peaks');
ylabel('Cumulative area fraction');
title('Area CDF with 80,90,95,97.5,99 percentiles');