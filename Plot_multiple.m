%plot multiple plots on top of each other
clc
clear
close all

cd('/Users/SamanthaD/Dropbox/UPLC analysis/Example chromatograms')

files={'MG1655_071812.mat','PBP2_transpep_mut_K12_C.mat','a1upm_Mec_K12_B.mat'};

peaks_to_align ={'M3','M4','D44'};

%housekeeping for alignment
t_peaks = zeros(size(files,2),3);
t_peaks_bef = zeros(size(files,2),3);
baseline_bef = zeros(size(files,2),3);
t_baseline_bef = zeros(size(files,2),3);
time_int = zeros(size(files,2),1);
add_baseline = zeros(size(files,2),3);
baseline = 0.0005;

A = struct([]);
color_p = ['b' 'r' 'g' 'k' 'm' 'y' 'c'];
for f=1:size(files,2)
    figure(1)
    A(f).data=load(files{f});
    time_int(f) = A(f).data.chrom.t(2)-A(f).data.chrom.t(1);
    A(f).data.chrom.t_adjust = A(f).data.chrom.t'; %these are going to be the ones that get shifted for alignment
    A(f).data.chrom.final_adjust = A(f).data.chrom.final(:,1)';
    A(f).data.chrom.coeff2_adjust = A(f).data.chrom.coeff2;
    A(f).data.Peaks_adjust = A(f).data.Peaks;
    hold on %this allows you to plot things on top of each other
    plot(A(f).data.chrom.t,A(f).data.chrom.final(:,1),color_p(f),A(f).data.chrom.coeff2(:,3),A(f).data.chrom.coeff2(:,1),[color_p(f) '.']) %plots
    %     the final chromatogram vs. time and the coefficients
    hold off
    %now label the peaks
    for i =1:size(A(f).data.Peaks,2)
        for j=1:size(A(f).data.Peaks(i).position,1)
            x = A(f).data.Peaks(i).position(j);
            y = A(f).data.Peaks(i).gaussian(j,1);
            [ai,bi]=min((A(f).data.chrom.coeff2(:,3)-x).^2+(A(f).data.chrom.coeff2(:,1)-y).^2);
            A(f).data.htxt(bi)= text(A(f).data.chrom.coeff2(bi,3),A(f).data.chrom.coeff2(bi,1), A(f).data.Peaks(i).name,'Clipping','on','Color',color_p(f));
            A(f).data.txt(bi).peak=A(f).data.Peaks(i).name;
        end
        for p=1:size(peaks_to_align,2)
            if strcmp(A(f).data.Peaks(i).name,peaks_to_align{p}) %find where the main peaks are to align later
                t_peaks(p,f) = A(f).data.Peaks(i).position(A(f).data.Peaks(i).gaussian(:,1)==max(A(f).data.Peaks(i).gaussian(:,1)));
                t_peaks_bef(p,f) = A(f).data.Peaks(i).position(1);
                pos_peaks = find(A(f).data.chrom.t<t_peaks_bef(p,f),1,'last');
                min_peak = A(f).data.chrom.final(pos_peaks,1);
                flag = 1;
                while flag
                    if pos_peaks>1 && A(f).data.chrom.final(pos_peaks,1)>A(f).data.chrom.final(pos_peaks-1,1)
                        pos_peaks = pos_peaks-1;
                    else
                        flag = 0;
                    end
                end
                %find position in chromatogram before and after peak of interest where
                %chromatogram goes to baseline - add 0s there and shift things over
                baseline_bef(p,f) = pos_peaks;
                add_baseline(p,f) = A(f).data.chrom.final(pos_peaks,1);
                t_baseline_bef(p,f) = A(f).data.chrom.t(pos_peaks);                
            end
        end
    end
end

figure(1)
xlabel('Elution time (min)')
ylabel('Intensity (A(f).data.U.)')

%% Align things

for p=1:size(peaks_to_align,2)
    np=find(t_peaks(p,:)==max(t_peaks(p,:)));
    for pp=1:size(files,2)
        if pp~=np %move things everywhere but not for the one that already has things last
            t_to_shift = t_peaks(p,np) - t_peaks(p,pp);
            num_to_shift = round(t_to_shift/time_int(pp));
            t = t_baseline_bef(p,pp)+time_int(pp):time_int(pp):t_baseline_bef(p,pp)+time_int(pp)*num_to_shift;
            c = zeros(1,num_to_shift)+add_baseline(p,pp);
            A(pp).data.chrom.t_adjust = cat(2,A(pp).data.chrom.t_adjust(1:baseline_bef(p,pp)),t,A(pp).data.chrom.t_adjust(baseline_bef(p,pp)+1:end)+t_to_shift);
            A(pp).data.chrom.final_adjust = cat(2,A(pp).data.chrom.final_adjust(1:baseline_bef(p,pp)),c,A(pp).data.chrom.final_adjust(baseline_bef(p,pp)+1:end));
            A(pp).data.chrom.coeff2_adjust(A(pp).data.chrom.coeff2_adjust(:,3)>t_baseline_bef(p,pp)-time_int(pp),3)=A(pp).data.chrom.coeff2_adjust(A(pp).data.chrom.coeff2_adjust(:,3)>t_baseline_bef(p,pp)-time_int(pp),3)+t_to_shift;
            for i =1:size(A(pp).data.Peaks_adjust,2)
                for j=1:size(A(pp).data.Peaks_adjust(i).position,1)
                    if A(pp).data.Peaks_adjust(i).position(j)>t_baseline_bef(p,pp)-time_int(pp)
                        A(pp).data.Peaks_adjust(i).position(j)=A(pp).data.Peaks_adjust(i).position(j)+t_to_shift;
                    end
                end
            end
            if p<size(peaks_to_align,2)
                t_peaks(p+1:end,pp)=t_peaks(p+1:end,pp)+t_to_shift;
                t_peaks_bef(p+1:end,pp)=t_peaks_bef(p+1:end,pp)+t_to_shift;
                baseline_bef(p+1:end,pp) = baseline_bef(p+1:end,pp)+num_to_shift;
                t_baseline_bef(p+1:end,pp) = t_baseline_bef(p+1:end,pp)+t_to_shift;
            end
        end
    end
end

%% now plot the aligned files
for f=1:size(files,2)
    figure(2)
    
    hold on %this allows you to plot things on top of each other
    plot(A(f).data.chrom.t_adjust,A(f).data.chrom.final_adjust,color_p(f),A(f).data.chrom.coeff2_adjust(:,3),A(f).data.chrom.coeff2_adjust(:,1),[color_p(f) '.']) %plots
    %     the final chromatogram vs. time and the coefficients
    hold off
    %now label the peaks
    for i =1:size(A(f).data.Peaks_adjust,2)
        for j=1:size(A(f).data.Peaks_adjust(i).position,1)
            x = A(f).data.Peaks_adjust(i).position(j);
            y = A(f).data.Peaks_adjust(i).gaussian(j,1);
            [ai,bi]=min((A(f).data.chrom.coeff2_adjust(:,3)-x).^2+(A(f).data.chrom.coeff2_adjust(:,1)-y).^2);
            A(f).data.htxt(bi)= text(A(f).data.chrom.coeff2_adjust(bi,3),A(f).data.chrom.coeff2_adjust(bi,1), A(f).data.Peaks(i).name,'Clipping','on','Color',color_p(f));
            A(f).data.txt(bi).peak=A(f).data.Peaks(i).name;
        end
    end
end
figure(2)
xlabel(['Elution time (min) ' files(1)])
ylabel('Intensity (A(f).data.U.)')