# README #
(see PDF file in dowload for further information)
Chromanalysis v. 1.0 Manual
Carolina Tropini, Samantha Desmarais, Amanda Miguel, Gabriel Billings, Kerwyn Casey Huang
November 20, 2015

**Introduction**

Welcome to Chromanalysis! The objective of this software is to simplify and automate the quantification of peak abundances in HPLC/UPLC chromatograms, particularly those focused on analyses of bacterial cell wall (peptidoglycan, or PG) composition. We’ve written a review of the application of HPLC to PG analysis that will familiarize you with our nomenclature (Desmarais et al, Molecular Microbiology 89: 1-13 (2013)). 

In the manual below, we’ve assumed a basic knowledge of Matlab, including how to run scripts, open files, and determine the values of variables.  We don’t think any advanced knowledge is needed, so if you’ve never used Matlab before, we suggest reading the introductory chapters of a Matlab book.

Note that text in the News Gothic MT font refers to either Matlab commands or filenames, and text in bold refers to a button or option in chromanalysis.

**Initiating data analysis**

First things first: you can download chromanalysis at: 
•	https://bitbucket.org/kchuanglab/chromanalysis/downloads

Click on “Download repository” to save the zip file and then unzip to a folder on your hard drive. 

This folder should have a movie chromanalylsis.mov that talks you through the entire analysis process described below.

Once the download has finished, open Matlab. (Note that all instructions below have been tested extensively in Matlab 2013a on a Mac; we expect that the instructions will work similarly on a Windows or Linux machine.) To add the chromanalysis folder to the Matlab path, click on *Set Path* (in MATLAB 2013, this is on the Home tab in the Environment section below Preferences):
 
click on Add with Subfolders and choose the folder. 
 
Click Save and then Close.
 
To open the graphical user interface (GUI), run the chromanalysis script. You can do this in one of two ways:
•	In the Command Window, type *chromanalysis* and then hit return.
•	Open the chromanalysis.m file, and then click on the *Run* button (green arrowhead) in the Matlab toolbar (in MATLAB 2013, this is on the Editor tab in the Run section):
 

To choose the dataset for analysis, click on the *Load & Label Chromatogram* button in the upper left corner and select a text file you would like to analyze in Chromanalysis.  Acceptable files include .arw, and .mat files.  
 
Hitting this button this button will immediately begin evaluation of your data with default settings. However, before initiating data analysis, you have three preprocessing options: blank subtraction, choice of standardization file, and percentage of data to include in the analysis window. Below is a description of each of these options.

**Blank subtraction**

Although the baseline absorbance should be zero in the absence of a sample, often a “blank” (solution without PG sample) will exhibit a nonzero baseline that should be removed before peak abundances are calculated. (Existing software programs often estimate this baseline by calculating the positions of the valleys surrounding a given peak, but this is prone to error especially when two peaks are close together.) 

We suggest actually measuring the absorbance of a blank for greatest accuracy. If you do so, the output file should be named with the format Blank_*asterisk*, where “*asterisk*” is the name of the chromatogram. For instance, if your chromatogram text file is named MG1655_example.arw, name the blank  Blank_MG1655_example.arw, and include it in the same folder as the chromatogram text file.  

To subtract the blank prior to analysis, click on the *Subtract Blank* checkbox before loading the chromatogram.  When *Load & Label* Chromatogram is then selected, chromanalysis will search for a blank with the appropriate label in the same folder as the chromatogram text file, and will perform a background subtraction to zero the baseline.  If *Subtract blank* is checked and the blank file is missing, you will get an error. Check proper spelling of your blank file and restart chromanalysis.

**Baseline calculation**

If a blank has not been run for a particular sample, or if signal is very low (such that the signal-to-noise ratio is low, thereby introducing spurious peaks during blank subtraction), chromanalysis can compute an approximate baseline that can be subtracted from the chromatogram.  To do so, check the *Alternative baseline* checkbox before loading the chromatogram.  Note that this computation will take longer to perform than subtracting a blank chromatogram.

**Data trimming**

In a typical separation, there is a salt peak that elutes at the very beginning, usually between 0.5 and 1 minute. To remove this data from the analysis, it is advisable to trim ~3% of the data from the beginning. The user can adjust the value of the % of initial data to cut off to fine tune the interval of the chromatographic run that will be considered for integration.  
 

**Standard comparison**

In order to automate peak identification, you need a standard to which peak elution times can be compared. We’ve supplied a standard for *Escherichia coli*, *Vibrio cholerae*, *Caulobacter crescentus* and *Pseudomonas aeruginosa*; other bacteria are likely to have similar peak identities but it’s a good idea to either check with published literature or perform mass spectrometry on the individual peaks.

The standard file that is loaded by default corresponds to the muropeptide peaks expected from an *E. coli* sample.  The standard lists peaks in order of appearance, and defines an expected window of retention times for each peak.  This Peak table will appear on the right-hand side of the window after the standard is loaded, and is interactive (see below).  
 

If you would like to analyze the peptidoglycan composition of a bacterium that differs substantially from *E. coli*, you can load a standard appropriate to that species by clicking on the *Load std* button.  
 
A dialog box will pop up to allow you to choose a .mat Matlab file that contains the information appropriate to that standard. 

If the chromatographic analysis has been performed on a bacterium for which no standard exists (i.e., the muropeptide composition is substantially different from *E. coli*), you have the option of creating a new standard.  
•	After loading the chromatogram of interest, click on the Create new std button.  
•	You will be prompted to *Enter first peak name*, which should be the first muropeptide peak you would like to add to the muropeptide standard profile.  
•	Enter the interval start and end times (in minutes) based on the chromatographic data.  Hit OK, and the new peak will appear in the Peak Table on the right-hand side.  
•	To add more peaks, Hit the *Add peak* button near the lower left-hand corner of the Peak table, and enter the name and interval times of each peak.  
•	If you would like to remove a peak from the Peak table, hit the *Remove peak* button and enter the peak name. The corresponding peak will be removed from the Peak table.   
•	To finalize the standard, press the Save analysis button for future use with analyses from that bacterial species.  

In order to update or add a peak to an existing standard (thereby avoiding the need to create a new standard from scratch):
•	Load the .mat file for the standard using the *Load & label* chromatogram button.  
•	Load the same Matlab standard file into the Peak table using the *Load std* button.  
•	Use the *Add peak* and *Remove peak* buttons as described above to add and remove peaks from the Peak table.  
•	Press *Save analysis* to update the standard file.  

**Working within the chromatogram window**

chromanalysis will initially label peaks using the retention times of the selected standard file.  Once loaded and labeled, colored lines will appear along the time axis of the chromatogram along with the peak names that have been assigned based on the expected elution intervals in the standard file. 
The user can zoom in, zoom out, pan, and use the data cursor to highlight points on the chromatogram by using the four buttons in the upper left-hand corner of the window.

The user can capture a screen shot of the chromatogram by hitting the *Save plot* button.  
 
A new window will appear labeled with the chromatogram name.  From this window, the user can modify the image using Matlab’s Plot tools functionality, and save the image in a variety of formats, such as a Matlab figure, or eps or pdf file. 

**Overlaying chromatograms**
The Overlay Chromatograms button can be used to overlay two Matlab chromatogram files. 
 
When loading the first chromatogram, make sure that MAT-files are enabled in the dialog window.
Select the .mat file corresponding to the chromatogram.  Then select the *Overlay chromatograms* button, ensure again that MAT-files is enabled, and select the second .mat chromatogram file.  The first chromatogram will be colored black, and the second chromatogram will be colored green.  In order to remove the overlaid chromatogram, hit the *Clear* button.  
 

**Options for visual presentation**

Several options are available for modifying the appearance of the chromatogram prior to saving the output:
 	 
•	Check or uncheck *Show labels* to include or exclude, respectively, muropeptide names from appearing next to peaks.  
•	Select the *Original chromatogram* button to overlay the raw data (without blank subtraction) from the chromatogram with the blank data.  The blank chromatogram will appear as a dashed magenta trace, with the raw chromatogram in black. 
•	Select the *Fitted chromatogram* button to show the final analyzed chromatogram, incorporating blank subtraction and smoothing.  
•	Select the *Blank button* to show the blank chromatogram used for baseline subtraction.  

**Optimizing labeling**

The initial attempt at labeling will likely have to be adjusted due to slight chromatographic variations in retention times from run to run.  Once the chromatogram is loaded, click on the Optimize labeling button to initiate alignment of the peak labeling in the chromatogram with the standard peak labels. 
 
This should result in more accurate peak assignments, which can be verified from the lengths of the colored lines below each peak that identify the time interval as a particular muropeptide.  

To further fine-tune peak integration intervals, retention times in the Peak table can be manually adjusted:
•	In the Peak table, click on the cell in the Interval start column in the row corresponding to the muropeptide you wish to adjust.  A cursor will appear.  
•	Click on the left side of the peak you wish to integrate, thereby selecting the starting retention time.  
•	Next, in the Peak table, click on the cell in the End (min) column in the muropeptide row.  
•	Click on the right side of the peak you wish to integrate with the data cursor, thereby selecting the ending retention time for the muropeptide.  
•	% area and % mol frac values for that peak update automatically based on the new integration.  
If you make a mistake while manually integrating, hit the Undo button in the upper left-hand side of the Peak table, and the table will revert to the previous interval of retention times.  
 

One of the strengths of chromanalysis is the ability to analyze and label multiple chromatogram files at once. To do so, use the *Batch Analysis* button.  
 
Clicking the *Batch Analysis* button will open a dialog window requiring the selection of a directory of raw .arw files. These files will all be processed using the standard chromatogram that is selected at the time. Files will be saved automatically to a folder named Analyzed in the current path. To the end of each analyzed filename, the string _to_check.mat is added to distinguish the file from one that is individually checked and analyzed. 

**Quantified results**

In the lower left quadrant of the chromanalysis window, a Results table outputs a number of quantifications calculated based on the integrated, labeled peaks. 
 
The *Calculation* column shows what peaks are used in a particular variable, the result is reported in the *Result* column, and the *Missing* column highlights peaks that are typically used in that calculation that are have not been labeled (and hence may be missing) in the chromatogram.  

Results are reported as percentage molar fractions when the *% MF* button is selected.  To change the results output to percentage of total area, select the % Area button instead.  

New formulas can be entered using the + button in the upper right-hand corner of the *Results* table.  Fomulas may be deleted using the - button in the upper right-hand corner of the *Results* table.  

Below is a description of each category within the table:  

1.	Crosslinking: calculated by summing all dimers and 2 × trimers to reflect the percentage of peptide stems that are crosslinked.  
2.	Glycan strand length: calculated by dividing 100 by the sum of all anhydro peaks, which represent the termini of glycan strands.
3.	Monomer: the percentage of monomers in the sample (denoted by peak labels starting with “M”).
4.	Dimer: the percentage of dimers in the sample (denoted by peak labels starting with “D”).
5.	Trimer: the percentage of trimers in the sample (denoted by peak labels starting with “T”).
6.	Lipoprotein: the percentage of lipoprotein degradation products in the sample (denoted by peak labels ending with “L”).
7.	Anhydro: the percentage of anhydro-capped glycan strands in the sample (denoted by peak labels ending with “N”).
8.	Dap-Dap: the percentage of multimers bonded through DAP-DAP linkages instead of the typical DAP-D-Ala crossbridge (denoted by peak labels ending with “D”).
9.	Pentapeptide: the percentage of muropeptides containing five amino acids in the peptide stem (denoted by monomer peak labels with “5”).
10.	 Pentaglyine: the percentage of muropeptides containing glycine instead of L-Ala at the first amino acid position in the peptide stem (denoted by peak labels ending with a “G”).