function [mon,dim,trim,lipo,anhy,dap,pentap,pentag] = muropeptide_calc(Peaks)
for i = 1: size(Peaks,2)
    Pname{i} = Peaks(i).name; 
    Init{i}=Peaks(i).name(1);
     P_area(i) = Peaks(i).area;
end
MF = P_area;
div2=strcmp(Init,'D');
MF(div2)=MF(div2)/2;
div3=strcmp(Init,'T');
MF(div3)=MF(div3)/3;

MF=MF/sum(MF);
mon = sum(MF(strcmp(Init,'M')));
dim = sum(MF(strcmp(Init,'D')));
trim = sum(MF(strcmp(Init,'T')));
lipo = sum(MF(strcmp(Pname,'M3L')|strcmp(Pname,'D43L')|strcmp(Pname,'D43LN')));
anhy = sum(MF(strcmp(Pname,'M3N')|strcmp(Pname,'M4N')|strcmp(Pname,'D34DN')|strcmp(Pname,'D43N')|strcmp(Pname,'D44N-1')|strcmp(Pname,'D44N-2')|strcmp(Pname,'D45N')|strcmp(Pname,'T443DN')|strcmp(Pname,'T443N')|strcmp(Pname,'T444N')|strcmp(Pname,'T445N')|strcmp(Pname,'M3NL')|strcmp(Pname,'D43LN')))+sum(MF(strcmp(Pname,'D44NN')))*2;
dap = sum(MF(strcmp(Pname,'D33D')|strcmp(Pname,'D34D')|strcmp(Pname,'D33DL')|strcmp(Pname,'T344D')|strcmp(Pname,'D34DN')|strcmp(Pname,'T443DN')));
pentap = sum(MF(strcmp(Pname,'M5')|strcmp(Pname,'D45')|strcmp(Pname,'T445')|strcmp(Pname,'D45N')|strcmp(Pname,'T445N')));
pentag = sum(MF(strcmp(Pname,'M4G')|strcmp(Pname,'D44G')));
