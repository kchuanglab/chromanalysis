function varargout = chromanalysis(varargin)
% CHROMANALYSIS M-file for chromanalysis.fig
%      CHROMANALYSIS, by itself, creates a new CHROMANALYSIS or raises the existing
%      singleton*.
%
%      H = CHROMANALYSIS returns the handle to a new CHROMANALYSIS or the handle to
%      the existing singleton*.
%
%      CHROMANALYSIS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CHROMANALYSIS.M with the given input arguments.
%
%      CHROMANALYSIS('Property','Value',...) creates a new CHROMANALYSIS or raises the
%      existing singleton*.  Starting from the left, property value pairs
%      are
%      applied to the GUI before chromanalysis_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to chromanalysis_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help chromanalysis

% Last Modified by GUIDE v2.5 25-Aug-2014 13:49:31

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @chromanalysis_OpeningFcn, ...
    'gui_OutputFcn',  @chromanalysis_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before chromanalysis is made visible.
function chromanalysis_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to chromanalysis (see VARARGIN)

% Choose default command line output for chromanalysis
handles.output = hObject;

handles.batch = 0;
handles.analyzed = 0;
handles.saving_plot = 0;
set(handles.label,'Value',1);
set(handles.subtract_blank,'Value',0);
set(handles.quant_smooth,'Value',0);
set(handles.blank_motif,'String','Blank_*');
set(handles.blank_motif,'Enable','off');
set(handles.Original_chrom,'Value',0);
set(handles.Fitted_chrom,'Value',1);
set(handles.blank,'Value',0);
% set(handles.min_blank,'Value',0);
set(handles.blank,'Visible','off');
% set(handles.min_blank,'Visible','off');
set(handles.perc,'String','3');
set(handles.uitable1,'ColumnWidth',{80 60 50 60});
set(handles.calculation_table,'ColumnWidth',{100 250 50 250});
set(handles.calculation_table,'ColumnName',{'Name','Calculation','Result','Missing'});
% set(handles.calculation_table,'ColumnWidth','auto')
set(handles.calculation_table,'ColumnFormat',{'char', 'char','numeric', 'char'});
handles.calculation = {'Crosslinking' '"D#"+2*"T#"' 0 '';'Glycan strand' '100/("M3N"+"M4N"+"D34DN"+"D43N"+"D44N-1"+"D44N-2"+"D45N"+"T443DN"+"T443N"+"T444N"+"T445N"+"M3NL"+"D43LN"+("D44NN")*2)' 0 '';
    'Monomer' '"M#"' 0 '';'Dimer' '"D#"' 0 '';'Trimer' '"T#"' 0 '';...
    'Lipoprotein' '"M3L"+"D43L"+"D43LN"' 0 '';...
    'Anhydro' '"M3N"+"M4N"+"D34DN"+"D43N"+"D44N-1"+"D44N-2"+"D45N"+"T443DN"+"T443N"+"T444N"+"T445N"+"M3NL"+"D43LN"+("D44NN")*2' 0 '';...
    'Dap-Dap' '"D33D"+"D34D"+"D33DL"+"T344D"+"D34DN"+"T443DN"' 0 '';...
    'Pentapeptide' '"M5"+"D45"+"T445"+"D45N"+"T445N"' 0 '';...
    'Pentaglycine' '"M4G"+"D44G"' 0 ''};

set(handles.calculation_table,'Data',handles.calculation);
set(handles.MF,'Value',1);
set(handles.Area,'Value',0);
% Update handles structure
guidata(hObject, handles);

% This sets up the initial plot - only do when we are invisible
% so window can get raised using chromanalysis.
load('Standard')
plot(chrom.t,chrom.final,'Parent',handles.axes1)
set(handles.axes1,'FontSize',10);
set(get(handles.axes1,'XLabel'),'String','Elution time (min)')
set(get(handles.axes1,'YLabel'),'String','Intensity (A.U.)')
set(handles.filename_txt,'String','');
set(handles.Batch_state,'String','');
handles.Peak_int = handles.standard_Peak_int;
set(handles.uitable1,'Data',handles.Peak_int)
guidata(hObject, handles);
%handle preferences (from file at first, then defaults if no file)
%Defaults based on wt E coli at 80 nm/pixel (4-2-2012)


% UIWAIT makes chromanalysis wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = chromanalysis_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in load_chrom.
function [handles] = load_chrom_Callback(hObject, eventdata, handles)
% hObject    handle to load_chrom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if handles.batch == 0;
    [file0,path0]=uigetfile({'*.arw';'*.mat'},'Select chromatogram');
    handles.file = file0;
    if file0==0;
        return
    end
    cd(path0)
else
    file0 = handles.file;
    path0 = handles.path;
    cd(path0)
end
handles.saving_plot = 0;
handles.batch = 0;
set(handles.filename_txt,'String',file0);
set(handles.standard_file,'ForegroundColor','r');
handles.save_plot_num = 0;
if numel(strfind(file0,'.arw'))>0
    params.dir = path0;
    params.perc = str2double(get(handles.perc,'String'));
    params.subtract_blank = get(handles.subtract_blank,'Value');
    params.blank_motif = get(handles.blank_motif,'String');
    params.quant_smooth=get(handles.quant_smooth,'Value');
    [chrom,baseline] = chromatogram_analysis(file0,params);
else
    if get(handles.subtract_blank,'Value')
        msgbox('Loading a pre-analyzed file will not add a baseline subtraction; if you want to subtract a baseline start with a raw data file.')
        set(handles.subtract_blank,'Value',0);%!!!!
    end
    load(fullfile(path0,file0))
end

if exist('chrom','var')
    handles.chrom = chrom;
    if exist('baseline','var')
        handles.baseline=baseline;
    else
        handles.baseline=[0,0];
    end
else
    %this is a hack for older files that used the name chrom2 instead of
    %chrom, could probably take it out
    handles.chrom = chrom2;
end

handles.total_area = trapz(handles.chrom.t,handles.chrom.final(:,1));
handles.chrom.coeff2(handles.chrom.coeff2(:,3)<0,:)=[];
if get(handles.Fitted_chrom,'Value');
    plot(handles.chrom.t,handles.chrom.final(:,1),'b','Parent',handles.axes1)
    hold on
    plot(handles.chrom.coeff2(:,3),handles.chrom.coeff2(:,1),'r.','Parent',handles.axes1)
    hold off
elseif get(handles.Original_chrom,'Value');
    plot(handles.chrom.orig_data(:,1),handles.chrom.orig_data(:,2),'b','Parent',handles.axes1)
    hold on
    plot(handles.baseline(:,1),handles.baseline(:,2),'-.m','Parent',handles.axes1)
    hold off
elseif get(handles.blank,'Value');
    plot(handles.chrom.orig_data(:,1),handles.chrom.blank(:,2),'b','Parent',handles.axes1)
% elseif get(handles.min_blank,'Value');
%     plot(handles.chrom.orig_data(:,1),handles.chrom.blank_sub_data(:,2),'b','Parent',handles.axes1)
end

set(handles.axes1,'FontSize',10);
set(get(handles.axes1,'XLabel'),'String','Elution time (min)')
set(get(handles.axes1,'YLabel'),'String','Intensity (A.U.)')

txt=struct('peak',[]);
handles.txt=txt;
for i=1:size(handles.chrom.coeff2,1)
    handles.txt(i).peak='';
    handles.htxt(i)= text(handles.chrom.coeff2(i,3), handles.chrom.coeff2(i,1), '','Clipping','on');
end
handles.search = 0;
if exist('Peaks','var')
    handles.search = 0;
    Pint=zeros(size(Peaks,2),3);
    for i = 1: size(Peaks,2)
        Pname{i} = Peaks(i).name;
        Pint(i,1) = 0;%don't label the peak position since might have multiple ones
        Pint(i,2:3) = Peaks(i).interval;
        Pint(i,4) = 0;
        Pint(i,5) = 0;
    end
    %%CT 92413
    [B,IX]=sort(Pint(:,2));
    Pint=Pint(IX,:);
    Pname=Pname(IX);
    Peaks=Peaks(IX);
    %%CT 92413
    
    handles.Peaks = Peaks;
    handles.Peak_int = Pint(:,2:5);
    handles.Peak_name = Pname;
    set(handles.uitable1,'RowName',Pname,'Data',handles.Peak_int);

    [handles] = Label_peaks_Callback(hObject, eventdata, handles);
else
    [handles] = Label_peaks_Callback(hObject, eventdata, handles);
end
handles.analyzed = 1;
if get(handles.subtract_blank,'Value')
    set(handles.blank,'Visible','on');
%     set(handles.min_blank,'Visible','on');
else
    set(handles.blank,'Visible','off');
%     set(handles.min_blank,'Visible','off');
end
guidata(hObject,handles)


% --- Executes on button press in save_analysis.
function save_analysis_Callback(hObject, eventdata, handles)
% hObject    handle to save_analysis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% ks=strfind(path3,'/');
ks = 0;
ks1 = strfind(handles.file,'.');
out_save = handles.file(max(ks)+1:ks1-1);
[ftemp,ptemp] = uiputfile([out_save '.mat'],'Save peaks analysis as ...');
Peaks=handles.Peaks;
chrom = handles.chrom;
calculations = get(handles.calculation_table,'Data');
save(fullfile(ptemp,ftemp),'Peaks','chrom','calculations');
% save tables also in an excel format
Peaks_table=get(handles.uitable1,'Data');
Peaks_table=[get(handles.uitable1,'ColumnName')';num2cell(Peaks_table)];
Peaks_table=[['Name';get(handles.uitable1,'RowName')] Peaks_table];
calculations = [get(handles.calculation_table,'ColumnName')';calculations];
fid=fopen(fullfile(ptemp,[ftemp(1:end-4) '.xls']),'w');
fprintf(fid,'%s\t%s\t%s\t%s\t%s\n', Peaks_table{1,:});
for i=2:size(Peaks_table,1)
    fprintf(fid,'%s\t%f\t%f\t%f\t%f\n', Peaks_table{i,:});
end
fprintf(fid,'\n\n');
fprintf(fid,'%s\t%s\t%s\t%s\n', calculations{1,:});
for i=2:size(calculations,1)
    fprintf(fid,'%s\t%s\t%f\t%s\n', calculations{i,:});
end
fclose(fid);

% --- Executes on button press in Label_peaks.
function [handles] = Label_peaks_Callback(hObject, eventdata, handles)
% hObject    handle to Label_peaks (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(handles.filename_txt,'String'),'')
    msgbox('Please load a chromatogram first','Error','error')
    return
end

if handles.saving_plot
    parent_ax = handles.axes_save; %plot on current axes (new figure from save_plot)
else
    parent_ax = handles.axes1; %plot on gui
    handles.v = axis;
end


guidata(hObject,handles)
handles.Peaks = peak_label(handles.chrom,handles.Peak_int,handles.Peaks,handles.search);
for i = 1: size(handles.Peaks,2)
    handles.Peak_int(i,1:2) = handles.Peaks(i).interval;
end


if isfield(handles,'second')
    if isfield(handles.second,'chrom2')
        handles.second.chrom = handles.second.chrom2;
    end
    if isfield(handles.second,'chrom')
        if get(handles.Fitted_chrom,'Value');
            plot(handles.chrom.t,handles.chrom.final(:,1),'k',handles.chrom.coeff2(:,3),handles.chrom.coeff2(:,1),'k.',handles.second.chrom.t,handles.second.chrom.final(:,1),'g',handles.second.chrom.coeff2(:,3),handles.second.chrom.coeff2(:,1),'g.','Parent',parent_ax)
            legend(get(handles.filename_txt(1),'String'),[get(handles.filename_txt(1),'String') ' peaks'],handles.file2,[handles.file2 ' peaks'])
        elseif get(handles.Original_chrom,'Value');
            plot(handles.chrom.orig_data(:,1),handles.chrom.orig_data(:,2),'k',handles.second.chrom.orig_data(:,1),handles.second.chrom.orig_data(:,2),'g','Parent',parent_ax)
            hold on
            plot(handles.baseline(:,1),handles.baseline(:,2),'-.m','Parent',parent_ax)
            hold off
            legend(get(handles.filename_txt(1),'String'),handles.file2,'Calculated baseline')
        elseif get(handles.blank,'Value');
            plot(handles.chrom.orig_data(:,1),handles.chrom.blank(:,2),'k','Parent',parent_ax)
%         elseif get(handles.min_blank,'Value');
%             plot(handles.chrom.orig_data(:,1),handles.chrom.blank_sub_data(:,2),'k','Parent',parent_ax)
        end
    else
        if get(handles.Fitted_chrom,'Value');
            plot(handles.chrom.t,handles.chrom.final(:,1),'k',handles.chrom.coeff2(:,3),handles.chrom.coeff2(:,1),'k.',handles.second(:,1),handles.second(:,2),'g','Parent',parent_ax)
            legend(get(handles.filename_txt(1),'String'),[get(handles.filename_txt(1),'String') ' peaks'],handles.file2)
        elseif get(handles.Original_chrom,'Value');
            plot(handles.chrom.orig_data(:,1),handles.chrom.orig_data(:,2),'k',handles.second(:,1),handles.second(:,2),'g','Parent',parent_ax)
            hold on
            plot(handles.baseline(:,1),handles.baseline(:,2),'-.m','Parent',parent_ax)
            hold off
            legend(get(handles.filename_txt(1),'String'),handles.file2,'Calculated baseline')
        elseif get(handles.blank,'Value');
            plot(handles.chrom.orig_data(:,1),handles.chrom.blank(:,2),'k','Parent',parent_ax)
%         elseif get(handles.min_blank,'Value');
%             plot(handles.chrom.orig_data(:,1),handles.chrom.blank_sub_data(:,2),'k','Parent',parent_ax)
        end
    end
else
    if get(handles.Fitted_chrom,'Value');
        plot(handles.chrom.t,handles.chrom.final(:,1),'k',handles.chrom.coeff2(:,3),handles.chrom.coeff2(:,1),'k.','Parent',parent_ax)
    elseif get(handles.Original_chrom,'Value');
        plot(handles.chrom.orig_data(:,1),handles.chrom.orig_data(:,2),'k','Parent',parent_ax)
        hold on
        plot(handles.baseline(:,1),handles.baseline(:,2),'-.m','Parent',parent_ax)
        hold off
    elseif get(handles.blank,'Value');
        plot(handles.chrom.orig_data(:,1),handles.chrom.blank(:,2),'k','Parent',parent_ax)
%     elseif get(handles.min_blank,'Value');
%         plot(handles.chrom.orig_data(:,1),handles.chrom.blank_sub_data(:,2),'k','Parent',parent_ax)
    end
end

if get(handles.label,'Value');
    hold on
    for i=1:numel(handles.Peaks)
        if mod(i,2) == 0
            c='r';
        else
            c='b';
        end
        %         c = hsv(numel(handles.Peaks));
        x = [handles.Peaks(i).interval(1) handles.Peaks(i).interval(2)];
        y = x./x*0-0.05;
        %         line(x,y,'LineStyle','-','LineWidth',4,'Parent',parent_ax,'Color',c(i,:));
        line(x,y,'LineStyle','-','LineWidth',4,'Parent',parent_ax,'Color',c);
        %         text(x(1)+(x(2)-x(1))/3,y(1)-0.035,handles.Peaks(i).name,'Parent',parent_ax,'Color',c(i,:),'Clipping','on');
        text(x(1)+(x(2)-x(1))/3,y(1)-0.035,handles.Peaks(i).name,'Parent',parent_ax,'Color',c,'Clipping','on');
        
    end
    hold off
end
for i =1:size(handles.Peaks,2)
    hold on
    for j=1:size(handles.Peaks(i).position,1)
        x = handles.Peaks(i).position(j);
        y = handles.Peaks(i).gaussian(j,1);
        [ai,bi]=min((handles.chrom.coeff2(:,3)-x).^2+(handles.chrom.coeff2(:,1)-y).^2);
        if get(handles.label,'Value');
            if y==max(handles.Peaks(i).gaussian(:,1))
                handles.htxt(bi)= text(handles.chrom.coeff2(bi,3), handles.chrom.coeff2(bi,1), handles.Peaks(i).name,'Parent',parent_ax,'Clipping','on');
            end
            if mod(i,2) == 0
                if y==max(handles.Peaks(i).gaussian(:,1))
                    set(handles.htxt(bi),'Color','r');
                end
                plot(x,y,'r.')
            else
                if y==max(handles.Peaks(i).gaussian(:,1))
                    set(handles.htxt(bi),'Color','b');
                end
                plot(x,y,'b.')
            end
        end
        handles.txt(bi).peak=handles.Peaks(i).name;
        A(j) = handles.chrom.coeff2(bi,1)*sqrt(2*pi)*abs(handles.chrom.coeff2(bi,2));
    end
    hold off
    if exist('A','var')
        handles.Peaks(i).area = sum(A)/handles.total_area;
        handles.Peak_int(i,3) = handles.Peaks(i).area*100;
    else
        handles.Peaks(i).area = 0;
        handles.Peaks(i).mol_frac = 0;
        handles.Peak_int(i,3) = 0;
        handles.Peak_int(i,4) = 0;
    end
    clear A
end
% calculate molar fraction
for i = 1: size(handles.Peaks,2)
    Pname{i} = handles.Peaks(i).name;
    Init{i}=handles.Peaks(i).name(1);
    P_area(i) = handles.Peaks(i).area;
end
MF = P_area;
div2=strcmp(Init,'D');
MF(div2)=MF(div2)/2;
div3=strcmp(Init,'T');
MF(div3)=MF(div3)/3;
MF=MF/sum(MF);
for i = 1:size(handles.Peaks,2)
    handles.Peaks(i).mol_frac= MF(i);
    handles.Peak_int(i,4) = handles.Peaks(i).mol_frac*100;
end
set(handles.uitable1,'Data',handles.Peak_int) %figure ok
set(get(parent_ax,'XLabel'),'String','Elution time (min)')
set(get(parent_ax,'YLabel'),'String','Intensity (A.U.)')
axis(handles.v);

% set(handles.axes1,'YLim',[-0.15 max(handles.chrom.final(:,1)+0.1)])
% set(handles.axes1,'XLim',[1.5 20])
% now calculate % crosslinking and glycan strand length
[mon,dim,trim,lipo,anhy,dap,pentap,pentag] = muropeptide_calc(handles.Peaks);
%Set up again also the calculation in the table
handles.calculation=get(handles.calculation_table,'Data');
for i=1:size(handles.calculation,1)
    patt_calc=handles.calculation{i,2};
    if ~isempty(patt_calc) %if editing the calculation column
        % check that there are the right number of parenthesis in the calculation
        [result_calc,missing]=peaks_calculation(patt_calc,handles);
        handles.calculation=get(handles.calculation_table,'Data');
        handles.calculation{i,3}=result_calc;
        handles.calculation{i,4}=missing;
        set(handles.calculation_table,'Data',handles.calculation);
    else
        handles.calculation{i,3}=0;
        set(handles.calculation_table,'Data',handles.calculation);
    end
end

guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function uitable1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to uitable1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Choose default command line output for chromanalysis
handles.uitable1 = hObject;

% Update handles structure
guidata(hObject, handles);

% set up initial peak positions
load(get(handles.standard_file,'String'));
%missing peak D45...?
handles.Peaks = Peaks;
Pint=zeros(size(Peaks,2),3);
for i = 1: size(Peaks,2)
    Pname{i} = Peaks(i).name;
    if ~isempty(Peaks(i).position)
        Pint(i,1) = Peaks(i).position(1);
    else
        Pint(i,1) =mean(Peaks(i).interval);
    end
    Pint(i,2:3) = Peaks(i).interval;
    Pint(i,4) = 0;
    Pint(i,5) = 0;
end
%%CT 92413
[B,IX]=sort(Pint(:,2));
Pint=Pint(IX,:);
Pname=Pname(IX);
Peaks=Peaks(IX);
%%CT 92413
handles.Peak_int = Pint(:,2:5);
handles.Peak_int_prev = handles.Peak_int;
handles.standard_Peak_int = handles.Peak_int;
handles.Peak_name = Pname;
columnname =   {'Interval start', 'end (min)','% Area','% Mol Frac'};
columneditable =  [false false];
set(handles.uitable1,'Units','normalized','Data',handles.Peak_int,'ColumnName', columnname,'RowName',Pname,'ColumnEditable', columneditable);
guidata(hObject,handles)


% --- Executes on button press in label_optimize.
function [handles] = label_optimize_Callback(hObject, eventdata, handles)
% hObject    handle to label_optimize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(handles.filename_txt,'String'),'')
    msgbox('Please load a chromatogram first','Error','error')
    return
end
Peaks = peak_to_name_alignment(handles.chrom,get(handles.standard_file,'String'));
Pint=zeros(size(Peaks,2),3);
for i = 1: size(Peaks,2)
    Pname{i} = Peaks(i).name;
    if ~isempty(Peaks(i).position)
        Pint(i,1) = Peaks(i).position(1);
    else
        Pint(i,1) =mean(Peaks(i).interval);
    end
    Pint(i,2:3) = Peaks(i).interval;
    Pint(i,4) = 0;
    Pint(i,5) = 0;
end
%%CT 92413
[B,IX]=sort(Pint(:,2));
Pint=Pint(IX,:);
Pname=Pname(IX);
Peaks=Peaks(IX);
%%CT 92413
handles.Peaks = Peaks;
handles.Peak_int = Pint(:,2:5);
handles.Peak_name = Pname;
set(handles.uitable1,'RowName',Pname,'Data',handles.Peak_int);
handles.search = 1;
[handles] = Label_peaks_Callback(hObject, eventdata, handles);
set(handles.standard_file,'ForegroundColor','k');
guidata(hObject,handles)


% --- Executes during object creation, after setting all properties.
function glycan_length_CreateFcn(hObject, eventdata, handles)
% hObject    handle to glycan_length (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% --- Executes during object creation, after setting all properties.
function crosslinked_CreateFcn(hObject, eventdata, handles)
% hObject    handle to crosslinked (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function Monomers_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Monomers (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function Dimers_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Dimers (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function Trimers_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Trimers (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function Lipoprotein_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Lipoprotein (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function Dapdap_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Dapdap (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function Pentapeptide_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Pentapeptide (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function Pentaglycine_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Pentaglycine (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function Anhydro_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Anhydro (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function axes1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes1
handles.axes1 = hObject;
guidata(hObject,handles)


% --- Executes during object creation, after setting all properties.
function filename_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to filename_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% handles.filename_txt=hObject;
% guidata(hObject,handles)


% --- Executes on button press in batch_analysis.
function batch_analysis_Callback(hObject, eventdata, handles)
% hObject    handle to batch_analysis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
folder_name = uigetdir(cd,'Select directory of .arw files to be processed');
if folder_name==0;
    return
end
cd (folder_name)

if ~exist('Analyzed', 'dir')
    mkdir('Analyzed')
end

handles.path = folder_name;
B=dir(fullfile(folder_name, '*.arw'));
b=size(B);

for nn=1:b(1)
    set(handles.Batch_state,'String',[num2str(nn) ' of ' num2str(b(1))]);
    handles.file = B(nn).name;
    handles.Peak_int = handles.standard_Peak_int;
    set(handles.uitable1,'Data',handles.Peak_int)
    handles.batch = 1;
    [handles] = load_chrom_Callback(hObject, eventdata, handles);
    [handles] = label_optimize_Callback(hObject, eventdata, handles);
    ks = 0;
    ks1 = strfind(handles.file,'.');
    out_save = handles.file(max(ks)+1:ks1-1);
    Peaks=handles.Peaks;
    chrom = handles.chrom;
    save([out_save '_to_check.mat'],'Peaks','chrom');
    movefile(B(nn).name,[folder_name '/Analyzed'])
end
handles.batch = 0;
guidata(hObject,handles)


% --- Executes during object creation, after setting all properties.
function Batch_state_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Batch_state (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes when selected cell(s) is changed in uitable1.
function uitable1_CellSelectionCallback(hObject, eventdata, handles)
% hObject    handle to uitable1 (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Indices: row and column indices of the cell(s) currently selecteds
% handles    structure with handles and user data (see GUIDATA)

sel = eventdata.Indices;
if exist('sel','var')&&numel(sel)~=0&&sel(2)<3
    x = ginput(1);
    if isempty(x)
        return
    end
    handles.Peak_int_prev = handles.Peak_int;
    handles.Peak_int(sel(1),sel(2)) = x(1);
    set(handles.uitable1,'Data',handles.Peak_int)
    if ~strcmp(handles.filename_txt,'')
        handles.search = 0;
        [handles] = Label_peaks_Callback(hObject, eventdata, handles);
    end
    guidata(hObject,handles)
    clear sel
end
h = pan;%(handles.axes1)
set(h,'Enable','on');


% --- Executes on button press in undo.
function undo_Callback(hObject, eventdata, handles)
% hObject    handle to undo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.Peak_int = handles.Peak_int_prev;
set(handles.uitable1,'Data',handles.Peak_int)
if ~strcmp(get(handles.filename_txt,'String'),'')
    handles.search = 0;
    [handles] = Label_peaks_Callback(hObject, eventdata, handles);
end
guidata(hObject,handles)
h = pan;%(handles.axes1)
set(h,'Enable','on');


% --- Executes during object creation, after setting all properties.
function standard_file_CreateFcn(hObject, eventdata, handles)
% hObject    handle to standard_file (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.standard_file=hObject;
set(handles.standard_file,'String','Standard.mat');
guidata(hObject,handles)

% --- Executes on button press in choose_std.
function choose_std_Callback(hObject, eventdata, handles)
% hObject    handle to choose_std (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[file0,path0]=uigetfile('*.mat','Select standard to align to');
if file0==0;
    return
end
%%%%%%%
set(handles.standard_file,'String',file0,'ForegroundColor','r');
guidata(hObject,handles)
addpath(path0);
[handles]=uitable1_Modify(hObject, eventdata, handles);
guidata(hObject,handles)


% --- Executes during object creation, after setting all properties.
function Labels_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Labels (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.label = hObject;
guidata(hObject,handles)

% --- Executes on button press in Labels.
function Labels_Callback(hObject, eventdata, handles)
% hObject    handle to Labels (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of Labels
[handles] = Label_peaks_Callback(hObject, eventdata, handles);
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function Original_chrom_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Original_chrom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.Original_chrom = hObject;
guidata(hObject,handles)

% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over Original_chrom.
function Original_chrom_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to Original_chrom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.Fitted_chrom,'Value',0);
set(handles.blank,'Value',0);
% set(handles.min_blank,'Value',0);
[handles] = Label_peaks_Callback(hObject, eventdata, handles);
guidata(hObject,handles)


% --- Executes during object creation, after setting all properties.
function Fitted_chrom_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Fitted_chrom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.Fitted_chrom = hObject;
guidata(hObject,handles)

% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over Fitted_chrom.
function Fitted_chrom_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to Fitted_chrom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.Original_chrom,'Value',0);
set(handles.blank,'Value',0);
% set(handles.min_blank,'Value',0);
[handles] = Label_peaks_Callback(hObject, eventdata, handles);
guidata(hObject,handles)


% --- Executes on button press in Original_chrom.
function Original_chrom_Callback(hObject, eventdata, handles)
% hObject    handle to Original_chrom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of Original_chrom
set(handles.Fitted_chrom,'Value',0);
% set(handles.min_blank,'Value',0);
set(handles.blank,'Value',0);
[handles] = Label_peaks_Callback(hObject, eventdata, handles);
guidata(hObject,handles)


% --- Executes on button press in Fitted_chrom.
function Fitted_chrom_Callback(hObject, eventdata, handles)
% hObject    handle to Fitted_chrom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of Fitted_chrom
set(handles.Original_chrom,'Value',0);
% set(handles.min_blank,'Value',0);
set(handles.blank,'Value',0);
[handles] = Label_peaks_Callback(hObject, eventdata, handles);
guidata(hObject,handles)


% --- Executes on button press in subtract_blank.
function subtract_blank_Callback(hObject, eventdata, handles)
% hObject    handle to subtract_blank (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.analyzed
    if get(handles.blank_motif,'Value')
        choice = questdlg('The file has already been analyzed subtracting the blank.', ...
            'New analysis will replace old unless saved.', ...
            'Analyze again','Cancel','Cancel');
    else
        choice = questdlg('The file has already been analyzed without dubtracting the blank.', ...
            'New analysis will replace old unless saved.', ...
            'Analyze again','Cancel','Cancel');
    end
    % Handle response
    switch choice
        case 'Analyze again'
            [handles] = load_chrom_Callback(hObject, eventdata, handles);
        case 'Cancel'
            if get(handles.blank_motif,'Value')
                set(handles.blank_motif,'Value',0)
            else
                set(handles.blank_motif,'Value',1)
            end
    end
end
guidata(hObject,handles)
% Hint: get(hObject,'Value') returns toggle state of subtract_blank



function blank_motif_Callback(hObject, eventdata, handles)
% hObject    handle to blank_motif (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of blank_motif as text
%        str2double(get(hObject,'String')) returns contents of blank_motif as a double


% --- Executes during object creation, after setting all properties.
function blank_motif_CreateFcn(hObject, eventdata, handles)
% hObject    handle to blank_motif (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.blank_motif = hObject;
guidata(hObject,handles)
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes during object creation, after setting all properties.
function subtract_blank_CreateFcn(hObject, eventdata, handles)
% hObject    handle to subtract_blank (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.subtract_blank = hObject;
guidata(hObject,handles)


% % --- Executes on button press in min_blank.
% function min_blank_Callback(hObject, eventdata, handles)
% % hObject    handle to min_blank (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% set(handles.Original_chrom,'Value',0);
% set(handles.Fitted_chrom,'Value',0);
% set(handles.blank,'Value',0);
% [handles] = Label_peaks_Callback(hObject, eventdata, handles);
% guidata(hObject,handles)
% % Hint: get(hObject,'Value') returns toggle state of min_blank


% --- Executes on button press in blank.
function blank_Callback(hObject, eventdata, handles)
% hObject    handle to blank (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.Original_chrom,'Value',0);
set(handles.Fitted_chrom,'Value',0);
% set(handles.min_blank,'Value',0);
[handles] = Label_peaks_Callback(hObject, eventdata, handles);
guidata(hObject,handles)
% Hint: get(hObject,'Value') returns toggle state of blank


% --- Executes during object creation, after setting all properties.
function blank_CreateFcn(hObject, eventdata, handles)
% hObject    handle to blank (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.blank = hObject;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
% function min_blank_CreateFcn(hObject, eventdata, handles)
% % hObject    handle to min_blank (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    empty - handles not created until after all CreateFcns called
% handles.min_blank = hObject;
% guidata(hObject,handles)



function perc_Callback(hObject, eventdata, handles)
% hObject    handle to perc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of perc as text
%        str2double(get(hObject,'String')) returns contents of perc as a double


% --- Executes during object creation, after setting all properties.
function perc_CreateFcn(hObject, eventdata, handles)
% hObject    handle to perc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.perc = hObject;
guidata(hObject,handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in overlay.
function overlay_Callback(hObject, eventdata, handles)
% hObject    handle to overlay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if strcmp(get(handles.filename_txt,'String'),'')
    msgbox('Please load a chromatogram first','Error','error')
    return
end

[file0,path0]=uigetfile({'*.arw';'*.mat'},'Select chromatogram');
if file0==0;
    return
end
filename = [path0 '/' file0];
handles.file2 = file0;
if numel(strfind(file0,'.arw'))>0
    handles.second = dlmread(filename);
else
    handles.second = load(filename);
end
guidata(hObject,handles)
[handles] = Label_peaks_Callback(hObject, eventdata, handles);
guidata(hObject,handles)



% --- Executes during object creation, after setting all properties.
function overlay_CreateFcn(hObject, eventdata, handles)
% hObject    handle to overlay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in clear_second.
function clear_second_Callback(hObject, eventdata, handles)
% hObject    handle to clear_second (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if isfield(handles,'second')
    [handles]=rmfield(handles, 'second');
    guidata(hObject,handles)
    [handles]=rmfield(handles, 'file2');
    guidata(hObject,handles)
    [handles] = Label_peaks_Callback(hObject, eventdata, handles);
    guidata(hObject,handles)
else
    msgbox('A second chromatogram needs to be loaded before clearing it!','Error','error')
    return
end


% --- Executes on button press in save_plot.
function save_plot_Callback(hObject, eventdata, handles)
% hObject    handle to save_plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% F=getframe(handles.axes1);               %select axes in GUI
% figure();                                          %new figure
% image(F.cdata);
handles.saving_plot = 1;
handles.v = axis;
b = get(handles.filename_txt(1),'String');
figure('Name',b(1:end-4));
handles.axes_save = gca;
guidata(hObject,handles)
[handles] = Label_peaks_Callback(hObject, eventdata, handles);
% saveas(gcf, [b(1:end-4) '_' num2str(handles.save_plot_num)], 'tiff');
print(gcf, [b(1:end-4) '_' num2str(handles.save_plot_num)], '-depsc','-tiff','-r400');
close(gcf);
handles.saving_plot = 0;
handles.save_plot_num = handles.save_plot_num+1;
guidata(hObject,handles)


% --------------------------------------------------------------------
function uitoggletool2_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uitoggletool2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.axes1,'YLim',get(handles.axes1,'YLim')*1.1)
set(handles.axes1,'XLim',get(handles.axes1,'xLim')*1.1)
guidata(hObject,handles)


% --- Executes during object creation, after setting all properties.
function [handles]=uitable1_Modify(hObject, eventdata, handles);
% hObject    handle to uitable1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Choose default command line output for chromanalysis
% set up initial peak positions
load(get(handles.standard_file,'String'));
%missing peak D45...?
handles.Peaks = Peaks;
Pint=zeros(size(Peaks,2),3);
for i = 1: size(Peaks,2)
    Pname{i} = Peaks(i).name;
    if ~isempty(Peaks(i).position)
        Pint(i,1) = Peaks(i).position(1);
    else
        Pint(i,1) =mean(Peaks(i).interval);
    end
    Pint(i,2:3) = Peaks(i).interval;
    Pint(i,4) = 0;
    Pint(i,5) = 0;
end
%%CT 92413
[B,IX]=sort(Pint(:,2));
Pint=Pint(IX,:);
Pname=Pname(IX);
Peaks=Peaks(IX);
handles.Peaks = Peaks;
%%CT 92413
handles.Peak_int = Pint(:,2:5);
handles.Peak_int_prev = handles.Peak_int;
handles.standard_Peak_int = handles.Peak_int;
handles.Peak_name = Pname;
guidata(hObject,handles)
%%%%%%
set(handles.uitable1,'RowName',Pname,'Data',handles.Peak_int);
guidata(hObject,handles)

% --- Executes on button press in add_peaks_to_std.
function add_peaks_to_std_Callback(hObject, eventdata, handles)
% hObject    handle to add_peaks_to_std (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ks=0;
a=get(handles.filename_txt(1),'String');
b=get(handles.standard_file,'String');
ks2=strfind(b,'.');
ks1 = strfind(a,'.');
if ~strcmp(a(max(ks)+1:ks1-1),b(max(ks)+1:ks2-1))
    msgbox('The standard needs to be loaded to edit it: names have to match','Error','error')
    return
end
prompt = {'Enter peak name:','Enter Interval Start:','Enter Interval End:'};
dlg_title = 'Input';
num_lines = 1;
def = {'','',''};
answer = inputdlg(prompt,dlg_title,num_lines,def);
N=size(handles.Peaks,2)+1;
handles.Peaks(N).name = answer{1,:};
handles.Peaks(N).interval(1) = str2double(answer(2,:));
handles.Peaks(N).interval(2) = str2double(answer(3,:));
overlp=1;
while overlp==1;
    overlp_check=1;
    %     for i = 1:size(handles.Peaks,2)
    i=size(handles.Peaks,2);
    for j=1:size(handles.Peaks,2)
        if i~=j&&((handles.Peaks(i).interval(1)<handles.Peaks(j).interval(2)&&handles.Peaks(i).interval(1)>handles.Peaks(j).interval(1))||(handles.Peaks(i).interval(2)<handles.Peaks(j).interval(2)&&handles.Peaks(i).interval(2)>handles.Peaks(j).interval(1)))
            prompt={[handles.Peaks(i).name ' start'],[handles.Peaks(i).name ' end'],[handles.Peaks(j).name ' start'],[handles.Peaks(j).name ' end']};
            dlg_title = ['Peaks ' handles.Peaks(i).name ' and ' handles.Peaks(j).name ' are overlapping'];
            num_lines = 1;
            def = {num2str(handles.Peaks(i).interval(1)),num2str(handles.Peaks(i).interval(2)),num2str(handles.Peaks(j).interval(1)),num2str(handles.Peaks(j).interval(2))};
            overlp_check=0;
            answer = inputdlg(prompt,dlg_title,num_lines,def);
            handles.Peaks(i).interval(1) = str2double(answer(1,:));
            handles.Peaks(i).interval(2) = str2double(answer(2,:));
            handles.Peaks(j).interval(1) = str2double(answer(3,:));
            handles.Peaks(j).interval(2) = str2double(answer(3,:));
        end
    end
    %     end
    if overlp_check
        overlp=0;
    end
end
Pint=zeros(size(handles.Peaks,2),3);
for i = 1: size(handles.Peaks,2)
    Pname{i} = handles.Peaks(i).name;
    if ~isempty(handles.Peaks(i).position)
        Pint(i,1) = handles.Peaks(i).position(1);
    else
        Pint(i,1) =mean(handles.Peaks(i).interval);
    end
    Pint(i,2:3) = handles.Peaks(i).interval;
    Pint(i,4) = 0;
    Pint(i,5) = 0;
end
%%CT 92413
[B,IX]=sort(Pint(:,2));
Pint=Pint(IX,:);
Pname=Pname(IX);
handles.Peaks=handles.Peaks(IX);
%%CT 92413
handles.Peak_int = Pint(:,2:5);
handles.Peak_int_prev = handles.Peak_int;
handles.standard_Peak_int = handles.Peak_int;
handles.Peak_name = Pname;
guidata(hObject,handles)
set(handles.uitable1,'RowName',Pname,'Data',handles.Peak_int);
[handles] = Label_peaks_Callback(hObject, eventdata, handles);
%%%need to add check for peak overlap
guidata(hObject,handles)


% --- Executes during object creation, after setting all properties.
function add_peaks_to_std_CreateFcn(hObject, eventdata, handles)
% hObject    handle to add_peaks_to_std (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in remove_peaks.
function remove_peaks_Callback(hObject, eventdata, handles)
% hObject    handle to remove_peaks (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ks=0;
a=get(handles.filename_txt(1),'String');
b=get(handles.standard_file,'String');
ks2=strfind(b,'.');
ks1 = strfind(a,'.');

if ~strcmp(a(max(ks)+1:ks1-1),b(max(ks)+1:ks2-1))
    msgbox('The standard needs to be loaded to edit it: names have to match','Error','error')
    return
end
prompt = {'Enter peak name:'};
dlg_title = 'Remove Peak (name)';
num_lines = 1;
def = {''};
answer = inputdlg(prompt,dlg_title,num_lines,def);
rem=0;
for i=1:size(handles.Peaks,2)
    if strcmp(answer{1,:},handles.Peaks(i).name)
        rem=i;
    end
end
if rem>0
    choice = questdlg(['Are you sure you want to remove peak ' answer{1,:}]);
    switch choice
        case 'Yes'
            if size(handles.Peaks,2)==rem
                handles.Peaks(rem)=[];
            else
                for i=1:size(handles.Peaks,2)-rem
                    handles.Peaks(rem)=handles.Peaks(rem+i);
                    if size(handles.Peaks,2)==rem+1
                        handles.Peaks(rem+1)=[];
                    end
                end
            end
        case 'Cancel'
            return
        case 'No'
            return
    end
else
    msgbox(['No peak was found with name ' answer{1,:}] ,'Error','error')
    return
end
Pint=zeros(size(handles.Peaks,2),3);
for i = 1: size(handles.Peaks,2)
    Pname{i} = handles.Peaks(i).name;
    if ~isempty(handles.Peaks(i).position)
        Pint(i,1) = handles.Peaks(i).position(1);
    else
        Pint(i,1) =mean(handles.Peaks(i).interval);
    end
    Pint(i,2:3) = handles.Peaks(i).interval;
    Pint(i,4) = 0;
    Pint(i,5) = 0;
end
[B,IX]=sort(Pint(:,2));
Pint=Pint(IX,:);
Pname=Pname(IX);
handles.Peaks=handles.Peaks(IX);
handles.Peak_int = Pint(:,2:5);
handles.Peak_int_prev = handles.Peak_int;
handles.standard_Peak_int = handles.Peak_int;
handles.Peak_name = Pname;
guidata(hObject,handles)
set(handles.uitable1,'RowName',Pname,'Data',handles.Peak_int);
[handles] = Label_peaks_Callback(hObject, eventdata, handles);
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function remove_peaks_CreateFcn(hObject, eventdata, handles)
% hObject    handle to remove_peaks (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in new_standard.
function new_standard_Callback(hObject, eventdata, handles)
% hObject    handle to new_standard (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(handles.filename_txt,'String'),'')
    msgbox('Please load a chromatogram first','Error','error')
    return
end
chrom = handles.chrom;
mid=mean([min(chrom.t) max(chrom.t)]);
ks = 0;
ks1 = strfind(handles.file,'.');
out_save = handles.file(max(ks)+1:ks1-1);
if strcmp(handles.file(ks1+1:end),'mat')
    choice = questdlg('Are you sure you want to substitute saved analysis?');
    switch choice
        case 'Yes'
            
        case 'Cancel'
            return
        case 'No'
            return
    end
end
prompt = {'Enter first peak name:','Enter Interval Start:','Enter Interval End:'};
dlg_title = 'Input';
num_lines = 1;
def = {'',num2str(mid-mid/2),num2str(mid+mid/2)};
answer = inputdlg(prompt,dlg_title,num_lines,def);
handles.Peaks(1).name = answer{1,:};
handles.Peaks(1).interval(1) = str2double(answer(2,:));
handles.Peaks(1).interval(2) = str2double(answer(3,:));
if any(handles.Peaks(1).interval<min(chrom.t))||any(handles.Peaks(1).interval>max(chrom.t))
    msgbox('You need to select a peak within the time bounds','Error','error')
    return
end

handles.Peaks(2:end)=[];
Peaks=handles.Peaks(1);
save([out_save '.mat'],'Peaks','chrom');
set(handles.standard_file,'String',[out_save '.mat']);
[handles]=uitable1_Modify(hObject, eventdata, handles);
[handles] = Label_peaks_Callback(hObject, eventdata, handles);
set(handles.standard_file,'ForegroundColor','b');
guidata(hObject,handles)


% --- Executes during object creation, after setting all properties.
function new_standard_CreateFcn(hObject, eventdata, handles)
% hObject    handle to new_standard (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in quant_smooth.
function quant_smooth_Callback(hObject, eventdata, handles)
% hObject    handle to quant_smooth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of quant_smooth


% --- Executes during object creation, after setting all properties.
function quant_smooth_CreateFcn(hObject, eventdata, handles)
% hObject    handle to quant_smooth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.quant_smooth = hObject;
guidata(hObject,handles)


% --- Executes when entered data in editable cell(s) in uitable2.
function uitable2_CellEditCallback(hObject, eventdata, handles)
% hObject    handle to uitable2 (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)
ind_table=eventdata.Indices;
handles.calculation=get(handles.calculation_table,'Data');
patt_calc=handles.calculation(ind_table(1),2);
patt_calc=patt_calc{:};
if ind_table(2)==2 && ~isempty(patt_calc) %if editing the calculation column
    % check that there are the right number of parenthesis in the calculation
    open_par = strfind(patt_calc,'(');
    close_par =strfind(patt_calc,')');
    if numel(open_par)~=numel(close_par)
        result_calc=0;
        handles.calculation{ind_table(1),3}=result_calc;
        set(handles.calculation_table,'Data',handles.calculation);
        error('The number of parenthesis does not match')
        %         uicontrol(handles.calculation) %go back to editing
        return
    end
    [result_calc,missing]=peaks_calculation(patt_calc,handles);
    handles.calculation=get(handles.calculation_table,'Data');
    handles.calculation{ind_table(1),3}=result_calc;
    handles.calculation{ind_table(1),4}=missing;
    set(handles.calculation_table,'Data',handles.calculation);
elseif isempty(patt_calc)
    result_calc=0;
    handles.calculation{ind_table(1),3}=result_calc;
    handles.calculation{ind_table(1),2}=patt_calc;
    set(handles.calculation_table,'Data',handles.calculation);
end
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function uitable2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to uitable2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.calculation_table = hObject;
guidata(hObject,handles)


% --- Executes on button press in add_calc.
function add_calc_Callback(hObject, eventdata, handles)
% hObject    handle to add_calc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.calculation=get(handles.calculation_table,'Data');
handles.calculation=cat(1,handles.calculation,{'' '' 0 ''});
set(handles.calculation_table,'Data',handles.calculation);
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function add_calc_CreateFcn(hObject, eventdata, handles)
% hObject    handle to add_calc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in remove_calc.
function remove_calc_Callback(hObject, eventdata, handles)
% hObject    handle to remove_calc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ks=0;
handles.calculation=get(handles.calculation_table,'Data');
names = handles.calculation(:,1);
prompt = {'Enter calculation name:'};
dlg_title = 'Remove calculation (Name)';
num_lines = 1;
def = {''};
answer = inputdlg(prompt,dlg_title,num_lines,def);
rem=0;
for i=1:size(names,1)
    if strcmp(answer{1,:},names(i,:))
        rem=i;
    end
end
if rem>0
    choice = questdlg(['Are you sure you want to remove calculation ' answer{1,:}]);
    switch choice
        case 'Yes'
            handles.calculation(rem,:)=[];
            set(handles.calculation_table,'Data',handles.calculation);
        case 'Cancel'
            return
        case 'No'
            return
    end
else
    msgbox(['No calculation was found with name ' answer{1,:}] ,'Error','error')
    return
end
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function remove_calc_CreateFcn(hObject, eventdata, handles)
% hObject    handle to remove_calc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in MF.
function MF_Callback(hObject, eventdata, handles)
% hObject    handle to MF (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of MF
set(handles.MF,'Value',1);
set(handles.Area,'Value',0);
%Set up again also the calculation in the table
handles.calculation=get(handles.calculation_table,'Data');
for i=1:size(handles.calculation,1)
    patt_calc=handles.calculation{i,2};
    if ~isempty(patt_calc) %if editing the calculation column
        % check that there are the right number of parenthesis in the calculation
        result_calc=peaks_calculation(patt_calc,handles);
        handles.calculation=get(handles.calculation_table,'Data');
        handles.calculation{i,3}=result_calc;
        set(handles.calculation_table,'Data',handles.calculation);
    else
        handles.calculation{i,3}=0;
        set(handles.calculation_table,'Data',handles.calculation);
    end
end
guidata(hObject,handles)

% --- Executes on button press in Area.
function Area_Callback(hObject, eventdata, handles)
% hObject    handle to Area (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of Area
set(handles.MF,'Value',0);
set(handles.Area,'Value',1);
%Set up again also the calculation in the table
handles.calculation=get(handles.calculation_table,'Data');
for i=1:size(handles.calculation,1)
    patt_calc=handles.calculation{i,2};
    if ~isempty(patt_calc) %if editing the calculation column
        % check that there are the right number of parenthesis in the calculation
        result_calc=peaks_calculation(patt_calc,handles);
        handles.calculation=get(handles.calculation_table,'Data');
        handles.calculation{i,3}=result_calc;
        set(handles.calculation_table,'Data',handles.calculation);
    else
        handles.calculation{i,3}=0;
        set(handles.calculation_table,'Data',handles.calculation);
    end
end
guidata(hObject,handles)


% --- Executes during object creation, after setting all properties.
function MF_CreateFcn(hObject, eventdata, handles)
% hObject    handle to MF (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.MF=hObject;
guidata(hObject,handles)


% --- Executes during object creation, after setting all properties.
function Area_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Area (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.Area=hObject;
guidata(hObject,handles)
