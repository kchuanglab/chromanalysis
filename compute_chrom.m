function [chrom] = compute_chrom(times,betas)

% sum over all gaussians with betas
chrom.data = zeros(size(times));
for j=1:size(betas,1)
    if betas(j,1)>0
        % find point closest to center
        chrom.data = chrom.data+gaussfit(betas(j,:),times);
    else
        betas(j,1) = 0;        
        betas(j,2) = 0.1;
    end
end

% compute chromatogram statistics

% sort all the peaks
f = find(betas(:,1)>0);
chrom.betas = betas(f,:);
[v,iv] = sort(chrom.betas(:,3));

chrom.amps = chrom.betas(iv,1);
chrom.sigmas = chrom.betas(iv,2);
chrom.centers = chrom.betas(iv,3);

chrom.areas = sqrt(2*pi*chrom.sigmas.^2).*chrom.amps;

% sort areas
[sort_areas,ind_areas] = sort(chrom.areas);
%[cdf_areas,area_bins] = ecdf(chrom.areas);

% calculate my own CDF
chrom.total_area = sum(chrom.areas);
chrom.cdf_area(1) = sort_areas(end)/chrom.total_area;
for q=2:numel(ind_areas)
    chrom.cdf_area(q) = chrom.cdf_area(q-1)+sort_areas(end-q+1)/chrom.total_area;
end
