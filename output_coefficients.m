% print out Gaussian coefficients from chromatogram fitting
function output_coefficients(betas,dopause)

for i=1:size(betas,1)
    fprintf('%3d: %6.4f %3.2f %4.2f\n',i,betas(i,1),betas(i,2),betas(i,3));
end

if dopause
    pause;
end